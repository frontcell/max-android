package se.max.android.locator.ui;

import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.microsoft.windowsazure.messaging.NotificationHub;
import com.microsoft.windowsazure.notifications.NotificationsManager;

import java.util.HashSet;
import java.util.Set;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.analytics.AnalyticsWrapper;
import se.max.android.locator.utils.AzureNotificationsHandler;
import se.max.android.locator.utils.AzureUtils;
import se.max.android.locator.utils.Logger;

/**
 * Created by pqv on 27/11/15.
 */
public class AzureActivity extends BaseActivity {

    protected static final String SENDER_ID = BuildConfig.SENDER_ID;
    protected static final String CONNECTION_STRING = BuildConfig.CONNECTION_STRING;
    protected static final String HUB_PATH = BuildConfig.HUB_PATH;
    private GoogleCloudMessaging gcm;
    private NotificationHub hub;
    private boolean registeredWithNotificationHubs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        NotificationsManager.handleNotifications(this, SENDER_ID, AzureNotificationsHandler.class);

        gcm = GoogleCloudMessaging.getInstance(this);

        hub = new NotificationHub(HUB_PATH, CONNECTION_STRING, this);

    }
    @SuppressWarnings("unchecked")
    protected void registerWithNotificationHubs() {
        new AsyncTask() {
            @Override
            protected Object doInBackground(Object... params) {
                Set<String> tags = null;
                String regid = null;
                try {
                    regid = gcm.register(SENDER_ID);
                    Logger.d("AzureActivity regid:" + regid);

                    tags = new HashSet<String>();
                    tags.add("Username:");
                    tags.add("Device:" + AzureUtils.getId(AzureActivity.this));
                    tags.add("Platform:" + "Google");
                    tags.add("AppVersion:" + AzureUtils.getAppVersion(AzureActivity.this));
                    tags.add("Manufacturer:" + AzureUtils.getManufacturer());
                    tags.add("Model:" + AzureUtils.getModel());
                    tags.add("ScreenWidth:" + AzureUtils.getScreenWidth(AzureActivity.this));
                    tags.add("ScreenHeight:" + AzureUtils.getScreenHeight(AzureActivity.this));
                    tags.add("Group:" + AzureUtils.getGroupTag(AzureActivity.this));
                    tags.add("Location:" + AzureUtils.getSanitizedLocation(getLocation()));

                    hub.register(regid, tags.toArray(new String[tags.size()]));
                } catch (Exception e) {
                    AnalyticsWrapper.Event event = AnalyticsWrapper.Event.REGISTER_PUSH_FAIL;
                    if (tags != null) {
                        event.setLabel(tags.toString());
                    } else {
                        event.setLabel("");
                    }

                    AnalyticsWrapper.trackEvent(AzureActivity.this, event);
                    return e;
                }

                return regid;
            }
            @Override
            protected void onPostExecute(Object result) {
                if (result instanceof String) {
                    registeredWithNotificationHubs = true;
                    Logger.d("AzureActivity Azure Registration complete: " + (String) result);
                    //Toast.makeText(AzureActivity.this, "Registred for push", Toast.LENGTH_LONG).show();
                }
                if (result instanceof Exception) {
                    Logger.d("Azure Registration failed: " + (Exception) result);
                }

            }
        }.execute(null, null, null);
    }

    protected boolean isRegisteredWithNotificationHubs() {
        return registeredWithNotificationHubs;
    }
}
