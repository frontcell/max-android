package se.max.android.locator.data.api.max.model;

import com.google.gson.annotations.SerializedName;

/**
 * Class that wraps a server error message
 * 
 * @author andershagsten
 * 
 */
public class Error {
    @SerializedName("errorCode")
    private int mCode = 0;

    @SerializedName("title")
    private String mTitle = null;

    @SerializedName("message")
    private String mMessage = null;

    @Override
    public String toString() {
        return "GError [mCode=" + mCode + ", mTitle=" + mTitle + ", mMessage=" + mMessage + "]";
    }

    /**
     * Returns the error title;
     * 
     * @return
     */
    public String getTitle() {
        return mTitle;
    }

    /**
     * Returns the error message
     * 
     * @return
     */
    public String getMessage() {
        return mMessage;
    }

    /**
     * Get the error code of this error
     * 
     * @return
     */
    public int getCode() {
        return mCode;
    }

}