package se.max.android.locator.application;

import android.app.Application;
import android.content.Context;

import com.adform.adformtrackingsdk.AdformTrackingSdk;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Logger.LogLevel;
import com.google.android.gms.analytics.Tracker;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.R;
import se.max.android.locator.analytics.AnalyticsWrapper;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MaxApp extends Application {

    private static final String ADFORM_APP_NAME = "Max Android";
    private static final int ADFORM_TRACKPOINT_ID = 48565;


    private Tracker mTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        // Adform integration.
        AdformTrackingSdk.setAppName(ADFORM_APP_NAME);
        AdformTrackingSdk.startTracking(this, ADFORM_TRACKPOINT_ID);

        // Calligraphy integration.
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("flama-basic-webfont.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    public Tracker getTracker() {
        // Lazy initialization.
        if (mTracker == null) {
            GoogleAnalytics ga = GoogleAnalytics.getInstance(this);
            if (BuildConfig.DEBUG) {
                ga.getLogger().setLogLevel(LogLevel.VERBOSE);
            }

            mTracker = ga.newTracker(AnalyticsWrapper.getAnalyticsKey());
            mTracker.enableAdvertisingIdCollection(true);
        }

        return mTracker;
    }

    public static MaxApp getMaxApp(Context context) {
        return (MaxApp) context.getApplicationContext();
    }
}
