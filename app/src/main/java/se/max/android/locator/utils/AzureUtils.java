package se.max.android.locator.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Point;
import android.location.Location;
import android.os.Build;
import android.provider.Settings;
import android.view.Display;
import android.view.WindowManager;

import java.util.Random;

public class AzureUtils {

    private static final String PREFS_AZURE = "prefsAzure";
    private static final String SETTINGS_PROPERTY_GROUP_TAG = "propertyGroupTag";
    
    public static String getAppVersion(Context context) {

        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return sanitize(packageInfo.versionName);
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    public static String getManufacturer() {
        return sanitize(Build.MANUFACTURER);
    }
    
    public static String getModel() {
        return sanitize(Build.MODEL);
    }
    
    public static String getId(Context context) {
        if (context == null) {
            throw new IllegalArgumentException("Context was null when creating unique id");
        }
        return sanitize(Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID));
    }
    
    public static String getGroupTag(Context context) {
        final SharedPreferences prefs = getAzurePreferences(context);
        String groupTag = prefs.getString(SETTINGS_PROPERTY_GROUP_TAG, "");
        if (groupTag.length() == 0) {
            Random random = new Random();
            groupTag = Integer.toString(random.nextInt(1000));
            storeGroupTag(context, groupTag);
        }
        
        return sanitize(groupTag);
    }
    
    public static String getSanitizedLocation(Location location) {
        if (location == null) {
            return sanitize("null");
        } else {
            return sanitize(location.getLatitude() + "_" + location.getLongitude());
        }
    }
    
    @SuppressLint("NewApi") public static String getScreenWidth(Context context) {
        
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        int width = 0;
        
    
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            width = size.x;
        } else {
            width = display.getWidth();
        }   
        return sanitize(Integer.toString(width));
    }
    
    @SuppressLint("NewApi") public static String getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        int height = 0;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            display.getSize(size);
            height = size.y;
        } else {
            height = display.getHeight();
        }   
        return sanitize(Integer.toString(height));
    }
    
    private static String sanitize(String tag) {
        String result = tag.replaceAll("[^a-zA-Z0-9\\.\\-_~:@#]", "");
        return result;
    }

    private static void storeGroupTag(Context context, String groupTag) {
        final SharedPreferences prefs = getAzurePreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SETTINGS_PROPERTY_GROUP_TAG, groupTag);
        editor.commit();
    }
    
    private static SharedPreferences getAzurePreferences(Context context) {
        return context.getSharedPreferences(PREFS_AZURE, Context.MODE_PRIVATE);
    }   
}
