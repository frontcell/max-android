package se.max.android.locator.ui.express;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;

import com.google.gson.Gson;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.R;
import se.max.android.locator.analytics.AnalyticsWrapper;
import se.max.android.locator.analytics.AnalyticsWrapper.PageView;
import se.max.android.locator.data.DataManager;
import se.max.android.locator.data.api.max.model.ResponseBase;
import se.max.android.locator.data.api.max.model.menu.MenuEntry;
import se.max.android.locator.data.api.max.model.menu.MenuResponse;
import se.max.android.locator.data.api.max.model.menu.ReferenceMenuResponse;
import se.max.android.locator.data.api.max.model.push.Push;
import se.max.android.locator.ui.AzureActivity;
import se.max.android.locator.ui.WebFragment;
import se.max.android.locator.ui.findmax.FindMaxActivity;
import se.max.android.locator.ui.settings.PreferenceActivity;
import se.max.android.locator.ui.widgets.DisableableSlidingPaneLayout;
import se.max.android.locator.utils.AzureUtils;
import se.max.android.locator.utils.Logger;
import se.max.android.locator.utils.PushManager;
import se.max.android.locator.utils.RatingManager;
import se.max.android.locator.utils.ReloadManager;
import se.max.android.locator.utils.SchemeUtils;

import static se.max.android.locator.ui.express.MenuFragment.MenuFragmentInteractions;

public class ExpressActivity extends AzureActivity implements MenuFragmentInteractions {

    private boolean mSkipTryGetLocation;
    private boolean mAnimationInProgress;
    private boolean mDeepLinkIntentAvailable;

    public static Intent makeIntent(Context context, Push push) {
        Intent intent = new Intent(context, ExpressActivity.class);
        Gson gson = new Gson();
        String json = gson.toJson(push);
        intent.putExtra(PUSH_DATA, json);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    private static final String EXPRESS_FRAGMENT_TAG_NAME_KEY = "ExpressFragmentTagNameKey";
    private static final String PUSH_DATA = "PushData";

    private DisableableSlidingPaneLayout mSlidingPaneLayout;
    private MenuFragment mMenuFragment;

    private String mExpressWebFragmentTagName;
    private View mSplashContainerLayout;
    private View mFragmentContainerLayout;
    private int mAnimationDuration = 1500; // in millis.

    private ReloadManager mReloadManager;
    private DataManager mDataManager;
    private RatingManager mRatingManager;

    @Override
    public void onDestroy() {
        Logger.d("ExpressActivity onDestroy()");
        super.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        Logger.d("ExpressActivity onSaveInstanceState()");
        savedInstanceState.putString(EXPRESS_FRAGMENT_TAG_NAME_KEY, mExpressWebFragmentTagName);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Logger.d("ExpressActivity onCreate()");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_express);

        mSplashContainerLayout = findViewById(R.id.splash_container);
        mFragmentContainerLayout = findViewById(R.id.fragment_container);

        // Initially hide the content view.
        mFragmentContainerLayout.setVisibility(View.INVISIBLE);


        // Check whether we're recreating a previously destroyed instance.
        // Do some ugly cleanup because process gets killed if
        // user revokes permission from settings.
        if (savedInstanceState != null) {
            ExpressWebFragment fragment = (ExpressWebFragment) getSupportFragmentManager().findFragmentByTag(savedInstanceState.getString(EXPRESS_FRAGMENT_TAG_NAME_KEY));
            fragment.nullOutListener();
            Logger.d("ExpressActivity Recreating a previously destroyed instance");
            int count = getSupportFragmentManager().getBackStackEntryCount();
            Logger.d("# fragments in backstack:" + count);
            for (int i = 0; i < count; i++) {
                Logger.d("popBackStack!");
                getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        } else {
            String  dataString = getIntent().getDataString();
            String express = "maxexpress://express/";

            if (dataString != null && dataString.startsWith(express)) {
                mDeepLinkIntentAvailable = true;
            }
        }

        mSlidingPaneLayout = (DisableableSlidingPaneLayout) findViewById(R.id.sliding_pane_layout);
        mSlidingPaneLayout.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelOpened(View panel) {

            }

            @Override
            public void onPanelClosed(View panel) {
                mSlidingPaneLayout.disable();
            }
        });

        mSlidingPaneLayout.setSliderFadeColor(Color.TRANSPARENT);
        mSlidingPaneLayout.setParallaxDistance(400);
        mSlidingPaneLayout.disable();

        mReloadManager = new ReloadManager();
        mRatingManager = new RatingManager(this);
        mRatingManager.incExpressPageviewCount();
        mDataManager = new DataManager(this) {
            @Override
            public void onDataLoaded(ResponseBase data) {
                Logger.d("ExpressActivity onDataLoaded() ");
                if (data == null || data.hasError()) {
                    handleError(data == null ? null : data.getError(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mDataManager.loadMenu();
                        }
                    }, null);

                    return;
                }

                String menuJson  = new Gson().toJson((MenuResponse) data);

                if (menuJson == null) {
                    finish();
                } else {
                    // Construct the menu data.

                    MenuResponse menuData = null;
                    if (BuildConfig.INJECT_SETTINGS) { // Inject settings options in menu.
                        Logger.d("INJECT_SETTINGS");
                        menuData = new Gson().fromJson(menuJson, ReferenceMenuResponse.class);
                    } else {
                        menuData = new Gson().fromJson(menuJson, MenuResponse.class);
                    }

                    Logger.d("native url source: " + BuildConfig.NATIVE_URL_SOURCE);
                    Logger.d("native url:" + menuData.getNativeStartUrl(ExpressActivity.this));
                    // Feed menu data to MenuFragment.
                    FragmentManager supportFragmentManager = getSupportFragmentManager();
                    mMenuFragment = (MenuFragment) supportFragmentManager.findFragmentById(R.id.MenuFragment);
                    mMenuFragment.setMenuData(menuData);

                    if (pushIntentAvailable() || deepLinkIntentAvailable()) {
                        initiateCrossfadeAnimation();
                        if (pushIntentAvailable()) {
                            handlePushIntent();
                        } else if (deepLinkIntentAvailable()) {
                            handleDeepLinkIntent();
                        }
//                        // Handle intent after the crossfade.
//                        if (mSplashContainerLayout.getVisibility() == View.VISIBLE && !mAnimationInProgress) {
//                            startCrossfadeAnimation(true);
//                        } else {
//                            if (pushIntentAvailable()) {
//                                handlePushIntent();
//                            } else if (deepLinkIntentAvailable()) {
//                                handleDeepLinkIntent();
//                            }
//                        }
                    } else {

                        ExpressWebFragment expressWebFragment = (ExpressWebFragment) getSupportFragmentManager().findFragmentByTag(mExpressWebFragmentTagName);
                        if (expressWebFragment == null) {
                            // Load ExpressWebFragment.
                            mReloadManager.reload();
                            Logger.d("ExpressActivity ExpressWebFragment.newInstance(" + menuData.getNativeStartUrl(ExpressActivity.this) + ")");
                            expressWebFragment = ExpressWebFragment.newInstance(menuData.getNativeStartUrl(ExpressActivity.this));
                            mExpressWebFragmentTagName = expressWebFragment.getTagName();
                            replaceCurrentFragmentWith(expressWebFragment, false);
                        } else {
                            if (mReloadManager.reload()) {
                                expressWebFragment.reload();
                            }
                        }
                    }
                }
            }
        };

        // Check orientation
        Configuration currConfig = getResources().getConfiguration();
        if (currConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {


        } else if (currConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {

        }
    }

    private void initiateCrossfadeAnimation() {
        mSplashContainerLayout.setVisibility(View.VISIBLE);
        mSplashContainerLayout.setAlpha(1.0f);

        mFragmentContainerLayout.setVisibility(View.INVISIBLE);
        mFragmentContainerLayout.setAlpha(0f);
    }

    private boolean deepLinkIntentAvailable() {
        return mDeepLinkIntentAvailable;
    }

    @Override
    public void proceedWithLocationAvailable() {
        Logger.d("ExpressActivity proceedWithLocationAvailable()");
        mReloadManager.setCurrent(ReloadManager.AVAILABLE);
        if (!isRegisteredWithNotificationHubs()) {
            Logger.d("ExpressActivity registerWithNotificatonHubs()");
            registerWithNotificationHubs();
        }
        mDataManager.loadMenu();
        mSkipTryGetLocation = true;
    }

    @Override
    public void proceedWithLocationUnAvailable() {
        Logger.d("ExpressActivity proceedWithLocationUnAvailable()");
        mReloadManager.setCurrent(ReloadManager.UNAVAILABLE);
        if (!isRegisteredWithNotificationHubs()) {
            Logger.d("ExpressActivity registerWithNotificatonHubs()");
            registerWithNotificationHubs();
        }
        mDataManager.loadMenu();
        mSkipTryGetLocation = true;
    }

    @Override
    protected void onResume() {
        Logger.d("ExpressActivity onResume()");
        super.onResume();
//        if (!mSkipTryGetLocation) {
//            tryGetLocation();
//        }
//
//        mSkipTryGetLocation = false;
        proceedWithLocationUnAvailable();
    }

    private void handlePushIntent() {
        Logger.d("ExpressActivity handlePushIntent()");

        Gson gson = new Gson();
        String json = getIntent().getStringExtra(PUSH_DATA);
        Push push = gson.fromJson(json, Push.class);
        PushManager.setConsumed(this, push);
        switch (push.getView()) {
            case "find": {
                startActivity(FindMaxActivity.makeIntent(this, mDataManager.getMenuResponseAsJsonString()));
                break;
            }

            case "express": {
                Object obj = getSupportFragmentManager().findFragmentByTag(mExpressWebFragmentTagName);
                mReloadManager.reload();
                ExpressWebFragment expressWebFragment = ExpressWebFragment.newInstance(push.getUrl());
                mExpressWebFragmentTagName = expressWebFragment.getTagName();

                replaceCurrentFragmentWith(expressWebFragment, obj == null ? false : true);
                break;
            }

            case "site": {
                Object obj = getSupportFragmentManager().findFragmentByTag(mExpressWebFragmentTagName);
                mReloadManager.reload();
                WebFragment newFragment = WebFragment.newInstance(push.getUrl());
                replaceCurrentFragmentWith(newFragment, obj == null ? false : true);
                break;

            }

            default:
                Logger.d("ExpressActivity unhandled push " + push.toString());
                break;
        }
    }

    private void handleDeepLinkIntent() {
        Logger.d("ExpressActivity handleDeepLinkIntent()");

        String dataString = getIntent().getDataString();
        String express = "maxexpress://express/";

        // Parse out the url.
        String s1 = dataString.substring(express.length());
        String scheme = s1.substring(0, s1.indexOf('/'));
        String path = s1.substring(scheme.length() + 1);
        String url = scheme + "://" + path;

        Object obj = getSupportFragmentManager().findFragmentByTag(mExpressWebFragmentTagName);
        mReloadManager.reload();
        ExpressWebFragment expressWebFragment = ExpressWebFragment.newInstance(url);
        mExpressWebFragmentTagName = expressWebFragment.getTagName();

        replaceCurrentFragmentWith(expressWebFragment, obj == null ? false : true);

        mDeepLinkIntentAvailable = false;
    }


    private boolean pushIntentAvailable() {
        Intent pushIntent = getIntent();
        boolean available = false;
        if (pushIntent != null && pushIntent.getStringExtra(PUSH_DATA) != null) {
            Gson gson = new Gson();
            String json = pushIntent.getStringExtra(PUSH_DATA);
            Push push = gson.fromJson(json, Push.class);
            available = !PushManager.isConsumed(this, push);
        }

        Logger.d("ExpressActivity pushIntentAvailable() " + available);
        return available;
    }

    @Override
    protected void onPause() {
        Logger.d("ExpressActivity onPause() " + "isFinishing " + (isFinishing() ? "true" : false));
        super.onPause();
    }


    // MenuFragment.MenuFragmentInteractions implementation.

    @Override
    public void onMenuGroupClicked(MenuEntry group) {
        Logger.d("Group selected: " + group);
    }

    @Override
    public void onMenuChildClicked(MenuEntry group, MenuEntry child) {
        Logger.d("Child " + child + " in group " + group + "selected");

        if (child != null) {
            String url = child.getUrl();
            SchemeUtils.Scheme scheme = SchemeUtils.getScheme(url);

            if (scheme != null) {
                switch (scheme) {
                    case MAX_APP:
                        handleMaxAppScheme(url);
                        break;
                    case MAX_EXPRESS:
                        break;
                    case HTTP:
                    case HTTPS:
                        handleHttpScheme(url);
                        break;
                }
            }
        }

        mSlidingPaneLayout.closePane();
        mSlidingPaneLayout.disable();
    }

    private void handleMaxAppScheme(String url) {
        Logger.d("ExpressActivity handleMaxAppScheme " + url);
        Uri uri = Uri.parse(url);
        String authority = uri.getAuthority();

        switch (authority) {
            case "location":
                startActivity(FindMaxActivity.makeIntent(this, mDataManager.getMenuResponseAsJsonString()));
                break;
            case "settings":
                startActivity(PreferenceActivity.makeIntent(this, mDataManager.getMenuResponse().getNativeStartUrl(this)));
                break;
            default:
                break;
        }
    }

    private void handleHttpScheme(String url) {
        Logger.d("ExpressActivity handleHttpScheme " + url);

        Uri uri = Uri.parse(url);
        String authority = uri.getAuthority();

        Uri nativeStart = Uri.parse(mDataManager.getMenuResponse().getNativeStartUrl(ExpressActivity.this));
        String nativeUrlAuthority = nativeStart.getAuthority();

        if (authority.equals(nativeUrlAuthority)) {
            onCloseMaxPage();
        } else {
            if (isWebFragmentOnTop()) {
                WebFragment webFragment = (WebFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTagNameStack().peek());
                webFragment.load(url);
            } else {
                WebFragment newFragment = WebFragment.newInstance(url);
                replaceCurrentFragmentWith(newFragment, true);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Logger.d("ExpressActivity onNewIntent()" + (intent == null ? "" : intent.toString()) + " " + (intent.getExtras() == null ? "" : intent.getExtras().toString()));
        super.onNewIntent(intent);
        setIntent(intent);

        String  dataString = intent.getDataString();
        String express = "maxexpress://express/";

        if (dataString != null && dataString.startsWith(express)) {
            mDeepLinkIntentAvailable = true;
        }
    }

     // WebFragmentInteractions implementation.
    @Override
    public void onCloseMaxPage() {
        Logger.d("ExpressActivity onCloseMaxPage()");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isWebFragmentOnTop()) {
                    getFragmentTagNameStack().pop();
                    getSupportFragmentManager().popBackStackImmediate();
                }
                setOrientationLock();
            }
        });
    }

    @Override
    public void onOpenNavigation() {
        Logger.d("ExpressActivity onOpenNavigation()");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSlidingPaneLayout.enable();
                mSlidingPaneLayout.openPane();
                AnalyticsWrapper.trackPageView(ExpressActivity.this, PageView.NAVIGATON_MENU);
            }
        });
    }

    @Override
    public void onPageLoaded(String url) {
        mFragmentContainerLayout.setVisibility(View.VISIBLE);
        mSplashContainerLayout.setVisibility(View.INVISIBLE);
//        Logger.d("ExpressActivity onPageLoaded() " + url);
//        if (mSplashContainerLayout.getVisibility() == View.VISIBLE && !mAnimationInProgress) {
//            mAnimationInProgress = true;
//            // Set the content view to 0% opacity but visible, so that it is visible
//            // (but fully transparent) during the animation.
//            mFragmentContainerLayout.setAlpha(0f);
//            mFragmentContainerLayout.setVisibility(View.VISIBLE);
//
//            // Animate the content view to 100% opacity, and clear any animation
//            // listener set on the view.
//            mFragmentContainerLayout.animate()
//                    .alpha(1.0f)
//                    .setDuration(mAnimationDuration)
//                    .setListener(null);
//
//
//            // Animate the loading view to 0% opacity. After the animation ends,
//            // set its visibility to GONE as an optimization step (it won't
//            // participate in layout passes, etc.)
//            mSplashContainerLayout.animate()
//                    .alpha(0f)
//                    .setDuration(mAnimationDuration)
//                    .setListener(new AnimatorListenerAdapter() {
//                        @Override
//                        public void onAnimationEnd(Animator animation) {
//                            mAnimationInProgress = false;
//                            mSplashContainerLayout.setVisibility(View.GONE);
//                        }
//                    });
//        }
    }

    @Override
    public String onAppendUrl(String url) {
        System.out.println("xxy onAppendUrl:" + url);
        Logger.d("ExpressActivity onAppendUrl() before:" + url);
        Uri uri = Uri.parse(url);

        Uri.Builder builder = new Uri.Builder();
        builder.scheme(uri.getScheme())
                .authority(uri.getAuthority())
                .path(uri.getPath())
                .fragment(uri.getFragment())
                .appendQueryParameter("udid", AzureUtils.getId(this))
                .appendQueryParameter("os", "Android")
                .appendQueryParameter("osversion", Integer.toString(Build.VERSION.SDK_INT))
                .appendQueryParameter("appversion", AzureUtils.getAppVersion(this));


        // TODO(VU):
        Location loc = new Location("test");
        loc.setLatitude(55.6060);
        loc.setLongitude(13.0038);
        if (loc != null) {
            builder.appendQueryParameter("lat", Double.toString(loc.getLatitude()));
            builder.appendQueryParameter("lng", Double.toString(loc.getLongitude()));
        } else {
            // Check if permission is granted.
            int permissionCheckInt = ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION);

            boolean permissionCheckOK = permissionCheckInt == PackageManager.PERMISSION_GRANTED;

            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            boolean gpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            String dialogQuery = "";
            if (!permissionCheckOK && !gpsProviderEnabled) {
                dialogQuery = "bade";
            } else if (permissionCheckOK && !gpsProviderEnabled) {
                dialogQuery = "platstjanster";
            } else if (!permissionCheckOK && gpsProviderEnabled) {
                dialogQuery= "platsuppgifter";
            }

            builder.appendQueryParameter("dialog", dialogQuery);
        }

        for (String query : uri.getQueryParameterNames()) {
            if (!query.equals("udid") &&
                    !query.equals("os") &&
                    !query.equals("osversion") &&
                    !query.equals("appversion") &&
                    !query.equals("lat") &&
                    !query.equals("lng") &&
                    !query.equals("dialog")) {
                builder.appendQueryParameter(query, uri.getQueryParameter(query));
            }
        }

        String appendedUrl = builder.build().toString();
        Logger.d("ExpressActivity onAppendUrl()  after:" + appendedUrl);
        return appendedUrl;
    }

    @Override
    public void onOpenUrl(String url) {
        SchemeUtils.Scheme scheme = SchemeUtils.getScheme(url);
        if (scheme != null) {
            switch (scheme) {
                case MAX_APP:
                    handleMaxAppScheme(url);
                    break;
                case MAX_EXPRESS:
                    break;
                case HTTP:
                case HTTPS:
                    handleHttpScheme(url);
                    break;
            }
        }
    }

    @Override
    public void onFindMax() {
        startActivity(FindMaxActivity.makeIntent(this, mDataManager.getMenuResponseAsJsonString()));
    }
}
