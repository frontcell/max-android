package se.max.android.locator.ui;

import android.support.v4.app.Fragment;

public abstract class BaseFragment extends Fragment {

    private String tagName;

    public boolean onBackPress() {
        return false;
    }

    public String getTagName() {
        if (tagName == null) {
            tagName = this.getClass().getSimpleName() + System.currentTimeMillis();
        }
        return tagName;
    }

}
