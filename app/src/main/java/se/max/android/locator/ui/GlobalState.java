package se.max.android.locator.ui;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Vu Phan on 2017-10-18.
 */

public class GlobalState {

    private static DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static StringBuilder stringBuilder = new StringBuilder();

    public static String getLogText() {
        return stringBuilder.toString();
    }

    public static void appendLogText(String text) {
        stringBuilder.append(dateFormat.format(new Date()) + "\n").append(text).append("\n\n");
    }
}
