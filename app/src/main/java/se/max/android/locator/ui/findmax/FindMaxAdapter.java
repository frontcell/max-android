package se.max.android.locator.ui.findmax;

import android.location.Location;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import se.max.android.locator.R;
import se.max.android.locator.data.api.max.model.location.MLocation;
import se.max.android.locator.utils.Logger;
import se.max.android.locator.data.api.max.model.menu.RestaurantAttribute;
import se.max.android.locator.utils.LocationUtils;
import se.max.android.locator.utils.MLocationComparators;

public class FindMaxAdapter extends BaseAdapter {
    
    private List<MLocation> mData = new ArrayList<>();
    private List<MLocation> mFilteredData = new ArrayList<MLocation>();
    private Comparator<MLocation> mComparator = MLocationComparators.BY_DISTANCE;
    private RestaurantAttribute mCurrentFilter = new RestaurantAttribute("all", "");
    private Location mUserLocation;

    public FindMaxAdapter() {

    }

    public void setUserPosition(Location userLocation) {
        mUserLocation = userLocation;

        Logger.d("Recalculating adapter due to new user position");
        if (mData.size() == 0) {
            Logger.d("Recalculating adapter but data is empty");
        } else {
            for (MLocation location : mData) {
                location.setDistanceToUser(LocationUtils.distanceBetween(mUserLocation, location));
            }
            Collections.sort(mData, mComparator);
            applyFilter(mCurrentFilter);
            notifyDataSetChanged();
        }
    }
    
    public void sortBy(Comparator<MLocation> comparator) {
        mComparator = comparator;
        Collections.sort(mData, mComparator);
        applyFilter(mCurrentFilter);
        notifyDataSetChanged();
    }
    
    public void setData(List<MLocation> locations) {

        mData = locations;
        if (mUserLocation != null) {
            setUserPosition(mUserLocation);
        }
        sortBy(mComparator);
    }

    public void applyFilter(RestaurantAttribute attribute) {
        mCurrentFilter = attribute;
        mFilteredData.clear();
        mFilteredData.addAll(mData);
        if (attribute.getId().equals("all")) {
            notifyDataSetChanged();
            return;
        }

        for (MLocation location : mData) {
            if (!location.getAttributes().contains(attribute)) {
                mFilteredData.remove(location) ;
            }
        }

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mFilteredData == null ? 0 : mFilteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return mFilteredData == null ? null : mFilteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = View.inflate(parent.getContext(), R.layout.listitem_findmax, null);
        }
        TextView name = (TextView) v.findViewById(R.id.Name);
        TextView streetAddress = (TextView) v.findViewById(R.id.StreetAdress);
        TextView distance = (TextView) v.findViewById(R.id.Distance);
        MLocation location = mFilteredData.get(position);
        
        if (location.getAppName().equals(location.getCity())) {
            name.setText(location.getAppName());
        } else {
            if (location.getAppName().length() == 0) {
                name.setText(location.getCity());
            }
            else {
                name.setText(location.getCity() + ", " + location.getAppName());
            }
            
        }
        streetAddress.setText(location.getAddress());
        distance.setText(location.getDistanceString());
        v.setTag(location);
        return v;
    }

}
