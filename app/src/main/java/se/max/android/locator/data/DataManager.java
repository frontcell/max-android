package se.max.android.locator.data;

import android.content.Context;

import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.max.android.locator.data.api.max.model.location.LocationResponse;
import se.max.android.locator.data.api.max.model.menu.MenuResponse;

import static se.max.android.locator.utils.Common.notNull;

/**
 * Created by pqv on 26/11/15.
 */
public abstract class DataManager extends BaseDataManager {

    public enum NativeUrlSource {NATIVE, SHARED_PREFS, BUILD_CONFIG}

    private MenuResponse menuResponse;
    private LocationResponse locationResponse;

    public DataManager(Context context) {
        super(context);
    }

    public void loadRestaurants() {
        Call<LocationResponse> call = getMaxApi().getRestaurants();
        call.enqueue(new Callback<LocationResponse>() {

            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                if (response.isSuccessful()) {
                    locationResponse = response.body();
                    onDataLoaded(response.body());
                } else {
                    onDataLoaded(null);
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                onDataLoaded(null);
            }
        });
    }

    public void loadMenu() {
        Call<MenuResponse> call = getMaxApi().getMenu();
        call.enqueue(new Callback<MenuResponse>() {
            @Override
            public void onResponse(Call<MenuResponse> call, Response<MenuResponse> response) {
                if (response.isSuccessful()) {
                    menuResponse = response.body();
                    onDataLoaded(response.body());
                } else {
                    onDataLoaded(null);
                }
            }

            @Override
            public void onFailure(Call<MenuResponse> call, Throwable t) {
                onDataLoaded(null);
            }
        });
    }

    public MenuResponse getMenuResponse() {
        return menuResponse;
    }

    public String getMenuResponseAsJsonString() {
        notNull(menuResponse, "menuResponse is null");
        Gson gson = new Gson();
        return gson.toJson(menuResponse);
    }

    public LocationResponse getLocationResponse() {
        return locationResponse;
    }
}
