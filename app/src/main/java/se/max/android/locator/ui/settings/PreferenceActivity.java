package se.max.android.locator.ui.settings;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.R;
import se.max.android.locator.data.DataManager;
import se.max.android.locator.ui.log.LogActivity;

/**
 * Created by pqv on 10/05/16.
 */
public class PreferenceActivity extends AppCompatActivity {

    public static final String PREFS_SETTINGS = "prefsSettings";
    public static final String SETTING_CUSTOM_URL = "customUrl";
    public static final String SETTING_CHOICE = "choice";
    public static final int SETTING_CHOICE_DEFAULT = 1;

    private View customUrlView;
    private int currentChoice;
    private EditText editText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        RadioGroup rg = (RadioGroup) findViewById(R.id.radioGroup);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton1:
                        currentChoice = 1;
                        customUrlView.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.radioButton2:
                        currentChoice = 2;
                        customUrlView.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.radioButton3:
                        currentChoice = 3;
                        customUrlView.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.radioButton4:
                        currentChoice = 4;
                        customUrlView.setVisibility(View.INVISIBLE);
                        break;
                    case R.id.radioButton5:
                        currentChoice = 5;
                        customUrlView.setVisibility(View.VISIBLE);
                        break;
                    default:
                        currentChoice = 0;
                        customUrlView.setVisibility(View.INVISIBLE);
                }
            }
        });


        customUrlView = findViewById(R.id.linearLayout);

        RadioButton r1 = (RadioButton) findViewById(R.id.radioButton1); // choice = 1 // android:text="https://ref-ace-online.max.se/" />

        RadioButton r2 = (RadioButton) findViewById(R.id.radioButton2); // choice = 2 // android:text="https://test-ace-online.max.se/" />

        RadioButton r3 = (RadioButton) findViewById(R.id.radioButton3); // choice = 3 // android:text="https://ace-online.max.se/" />

        RadioButton r4 = (RadioButton) findViewById(R.id.radioButton4); // choice = 4 // android:text="native" />

        RadioButton r5 = (RadioButton) findViewById(R.id.radioButton5); // choice = 5 // android:text="custom" />


        SharedPreferences prefs = getSharedPreferences(PREFS_SETTINGS, Context.MODE_PRIVATE);

        final int currentChoice = prefs.getInt(SETTING_CHOICE, SETTING_CHOICE_DEFAULT);
        final String currentCustomUrl = prefs.getString(SETTING_CUSTOM_URL, "");

        switch (currentChoice) {
            case 1:
                r1.setChecked(true);
                break;
            case 2:
                r2.setChecked(true);
                break;
            case 3:
                r3.setChecked(true);
                break;
            case 4:
                r4.setChecked(true);
                break;
            case 5:
                r5.setChecked(true);
                break;
            default:
                break;
        }

        editText = (EditText) findViewById(R.id.editText);
        editText.setText(currentCustomUrl);

        if (BuildConfig.NATIVE_URL_SOURCE != DataManager.NativeUrlSource.SHARED_PREFS) {
            r1.setEnabled(false);
            r2.setEnabled(false);
            r3.setEnabled(false);
            r4.setEnabled(false);
            r5.setEnabled(false);

            TextView tv = (TextView) findViewById(R.id.urlSourceTextView);
            tv.setVisibility(View.VISIBLE);
            tv.setText(
                    "Url källa:\n" +
                            "BuildConfig." + BuildConfig.NATIVE_URL_SOURCE + "\n"
                            + getIntent().getStringExtra("native_url"));
        }


        Button save = (Button) findViewById(R.id.button);

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChoiceToPrefs();
            }
        });

        Button viewLog = (Button) findViewById(R.id.button2);

        viewLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLog();
            }
        });
    }

    private void showLog() {
        startActivity(new Intent(getApplicationContext(), LogActivity.class));
    }

    private void saveChoiceToPrefs() {
        SharedPreferences prefs = getSharedPreferences(PREFS_SETTINGS, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();


        if (currentChoice == 5) { // custom
            boolean valid = URLUtil.isValidUrl(editText.getText().toString());
            if (valid) {
                editor.putInt(SETTING_CHOICE, currentChoice);
                editor.putString(SETTING_CUSTOM_URL, editText.getText().toString());
                editor.commit();
                android.os.Process.killProcess(android.os.Process.myPid());
            } else {
                Snackbar.make(getWindow().getDecorView().getRootView().findViewById(android.R.id.content), "'" + editText.getText().toString() + "' är en ogiltig url", Snackbar.LENGTH_LONG).show();
            }

        } else {
            editor.putInt(SETTING_CHOICE, currentChoice);
            editor.commit();
            android.os.Process.killProcess(android.os.Process.myPid());
        }


    }

    public static Intent makeIntent(Context context, String nativeUrl) {
        Intent intent = new Intent(context, PreferenceActivity.class);
        intent.putExtra("native_url", nativeUrl);
        return intent;
    }

    @Override
    public void onResume() {
        super.onResume();

    }
}
