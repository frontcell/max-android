package se.max.android.locator.ui.express;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;

import java.util.ArrayList;
import java.util.List;

import se.max.android.locator.R;
import se.max.android.locator.data.api.max.model.menu.MenuEntry;
import se.max.android.locator.data.api.max.model.menu.MenuResponse;
import se.max.android.locator.utils.Logger;

public class MenuFragment extends Fragment {

    private ExpandableListView mMenuList;
    private MenuFragmentInteractions mListener;

    public interface MenuFragmentInteractions {
        /**
         * Callback when a group is expanded or closed
         * 
         * @param group
         */
        public void onMenuGroupClicked(MenuEntry group);

        /**
         * Callback when a child is clicked.
         * 
         * @param group
         *            The group this child belongs to
         * @param child
         *            The child that was clicked
         */
        public void onMenuChildClicked(MenuEntry group, MenuEntry child);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (MenuFragmentInteractions) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement MenuFragmentInteractions");
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Logger.d("MenuFragment:  onActivityCreated()");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View contentView = inflater.inflate(R.layout.fragment_menu, container, false);
        mMenuList = (ExpandableListView) contentView.findViewById(R.id.MenuList);
        View header = LayoutInflater.from(getActivity()).inflate(R.layout.divider, null, false);
        mMenuList.addHeaderView(header);
        mMenuList.setEmptyView(contentView.findViewById(android.R.id.empty));
        mMenuList.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                    int childPosition, long id) {
                boolean handled = false;
                if (mListener != null) {
                    final ExpandableListAdapter expandableListAdapter = parent.getExpandableListAdapter();
                    final MenuEntry group = (MenuEntry) expandableListAdapter.getGroup(groupPosition);
                    final MenuEntry child = (MenuEntry) expandableListAdapter.getChild(groupPosition,
                            childPosition);
                    mListener.onMenuChildClicked(group, child);
                    handled = true;
                }
                return handled;
            }
        });
        mMenuList.setOnGroupClickListener(new OnGroupClickListener() {

            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                boolean handled = false;
                if (mListener != null) {
                    final MenuEntry group = (MenuEntry) parent.getExpandableListAdapter().getGroup(
                            groupPosition);
                    if (group.getChildCount() > 0) {
                        mListener.onMenuGroupClicked(group);
                    } else {
                        mListener.onMenuChildClicked(null, group);
                    }

                }
                return handled;
            }
        });

        return contentView;
    }

    public void setMenuData(MenuResponse menuData) {
        List<MenuEntry> menuEntries = new ArrayList<MenuEntry>();
        MenuEntry root = menuData.getRoot();
        MenuEntry dummyHeader = new MenuEntry();
        dummyHeader.setGuid(root.getGuid());
        dummyHeader.setTitle(root.getTitle());
        dummyHeader.setUrl(root.getUrl());
        menuEntries.add(dummyHeader);
        menuEntries.addAll(menuData.getRoot().getChildren());
        mMenuList.setAdapter(new MenuAdapter(menuEntries));
    }

}
