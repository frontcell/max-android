package se.max.android.locator.ui.findmax;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SlidingPaneLayout;
import android.view.View;

import com.google.gson.Gson;

import se.max.android.locator.R;
import se.max.android.locator.analytics.AnalyticsWrapper;
import se.max.android.locator.data.api.max.model.menu.MenuEntry;
import se.max.android.locator.data.api.max.model.menu.MenuResponse;
import se.max.android.locator.ui.BaseActivity;
import se.max.android.locator.ui.WebFragment;
import se.max.android.locator.ui.express.MenuFragment;
import se.max.android.locator.ui.settings.PreferenceActivity;
import se.max.android.locator.ui.widgets.DisableableSlidingPaneLayout;
import se.max.android.locator.utils.Common;
import se.max.android.locator.utils.Logger;
import se.max.android.locator.utils.SchemeUtils;

import static se.max.android.locator.utils.Common.notNull;

/**
 * Created by pqv on 26/11/15.
 */
public class FindMaxActivity extends BaseActivity implements FindMaxFragment.FindMaxFragmentInteractions, WebFragment.WebFragmentInteractions, MenuFragment.MenuFragmentInteractions {

    private static final String NAVIGATION_DATA = "navigationData";

    private DisableableSlidingPaneLayout mSlidingPaneLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_findmax);

        int top = Common.getStatusBarHeight(this);
        findViewById(R.id.menu_container).setPadding(0, top, 0, 0);

        mSlidingPaneLayout = (DisableableSlidingPaneLayout) findViewById(R.id.sliding_pane_layout);
        mSlidingPaneLayout.setPanelSlideListener(new SlidingPaneLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelOpened(View panel) {

            }

            @Override
            public void onPanelClosed(View panel) {
                mSlidingPaneLayout.disable();
            }
        });

        mSlidingPaneLayout.setSliderFadeColor(Color.TRANSPARENT);
        mSlidingPaneLayout.setParallaxDistance(400);
        mSlidingPaneLayout.disable();

        Intent intent = notNull(getIntent(), "intent is null");
        String menuJson = notNull(intent.getStringExtra(NAVIGATION_DATA), "menuJson is null");

        // Construct the menu data.
        MenuResponse menuData = new Gson().fromJson(menuJson, MenuResponse.class);

        // Feed menu data to MenuFragment.
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        MenuFragment menuFragment = (MenuFragment) supportFragmentManager.findFragmentById(R.id.MenuFragment);
        menuFragment.setMenuData(menuData);

        // Create new fragment.
        FindMaxFragment locationFragment = new FindMaxFragment().newInstance(menuJson);
        replaceCurrentFragmentWith(locationFragment, false);
    }

    public static Intent makeIntent(Context context, String data) {
        Intent intent = new Intent(context, FindMaxActivity.class);
        intent.putExtra(NAVIGATION_DATA, data);
        return intent;
    }

    private void handleMaxAppScheme(String url) {
        Logger.d("FindMaxActivity handleMaxAppScheme " + url);
        Uri uri = Uri.parse(url);
        String authority = uri.getAuthority();

        switch (authority) {
            case "location":
                popBackStack();
                break;
            case "settings":
                startActivity(PreferenceActivity.makeIntent(this, "null"));
                break;
            default:
                break;
        }
    }


    @Override
    public void onOpenNavigation() {
        Logger.d("FindMaxActivity onOpenNavigation()");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mSlidingPaneLayout.enable();
                mSlidingPaneLayout.openPane();
                AnalyticsWrapper.trackPageView(FindMaxActivity.this, AnalyticsWrapper.PageView.NAVIGATON_MENU);
            }
        });
    }

    @Override
    public void onCloseMaxPage() {
        Logger.d("FindMaxActivity onCloseMaxPage()");
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (isWebFragmentOnTop()) {
                    getFragmentTagNameStack().pop();
                    getSupportFragmentManager().popBackStackImmediate();
                }
                setOrientationLock();
            }
        });
    }

    // FindMaxFragment FindMaxFragmentInteractions implementation.
    @Override
    public void onLocationClicked(String locationUrl) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int top = Common.getStatusBarHeight(this);
            findViewById(R.id.fragment_container).setPadding(0, top, 0, 0);
        }

        if (isWebFragmentOnTop()) {
            WebFragment webFragment = (WebFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTagNameStack().peek());
            webFragment.load(locationUrl);
        } else {
            WebFragment newFragment = WebFragment.newInstance(locationUrl);
            replaceCurrentFragmentWith(newFragment, true);
        }
    }

    @Override
    public void onOpenUrl(String url) {
        SchemeUtils.Scheme scheme = SchemeUtils.getScheme(url);
        if (scheme != null) {
            switch (scheme) {
                case MAX_APP:
                    handleMaxAppScheme(url);
                    break;
                case MAX_EXPRESS:
                case HTTP:
                case HTTPS:
                    break;
            }
        }
    }

    // MenuFragment MenuFragmentInteractions implementation.
    @Override
    public void onMenuGroupClicked(MenuEntry group) {

    }

    @Override
    public void onMenuChildClicked(MenuEntry group, MenuEntry child) {
        Logger.d("Child " + child + " in group " + group + "selected");

        if (child != null) {
            String url = child.getUrl();
            SchemeUtils.Scheme scheme = SchemeUtils.getScheme(url);

            if (scheme != null) {
                switch (scheme) {
                    case MAX_APP:
                        handleMaxAppScheme(url);
                        break;
                    case MAX_EXPRESS:
                        break;
                    case HTTP:
                    case HTTPS:
                        handleHttpScheme(url);
                        break;
                }
            }
        }

        mSlidingPaneLayout.closePane();
        mSlidingPaneLayout.disable();
    }

    private void handleHttpScheme(String url) {
        Logger.d("FindMaxActivity handleHttpScheme " + url);

        if (isWebFragmentOnTop()) {
            WebFragment webFragment = (WebFragment) getSupportFragmentManager().findFragmentByTag(getFragmentTagNameStack().peek());
            webFragment.load(url);
        } else {
            WebFragment newFragment = WebFragment.newInstance(url);
            replaceCurrentFragmentWith(newFragment, true);
        }
    }
}
