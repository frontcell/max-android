package se.max.android.locator.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.microsoft.windowsazure.notifications.NotificationsHandler;

import se.max.android.locator.R;
import se.max.android.locator.data.api.max.model.push.Push;
import se.max.android.locator.ui.express.ExpressActivity;

public class AzureNotificationsHandler extends NotificationsHandler {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    Context context;
    
    @Override
    public void onReceive(Context context, Bundle bundle) {
        this.context = context;
        String nhMessage = bundle.getString("msg");
        String nhView = bundle.getString("view");
        String nhUrl = bundle.getString("url");

        Logger.d("Received push notification: " + (nhMessage != null ? nhMessage : ""));

        Push push = new Push(nhView, nhUrl, nhMessage);

        sendNotification(push);
    }

    private void sendNotification(Push push) {
        mNotificationManager = (NotificationManager)
                  context.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = ExpressActivity.makeIntent(context, push);

        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
              intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ) {
                mBuilder.setSmallIcon(R.drawable.ic_notification_small_lollipop);
            } else {
                mBuilder.setSmallIcon(R.drawable.ic_notification_small);
            }
        mBuilder.setContentTitle("Express")
                .setDefaults(Notification.DEFAULT_SOUND)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(push.getMsg()))
                .setContentText(push.getMsg());

         mBuilder.setContentIntent(contentIntent).setAutoCancel(true);
         mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

}
