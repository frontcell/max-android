package se.max.android.locator.data;

import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import se.max.android.locator.data.api.max.MaxService;
import se.max.android.locator.data.api.max.model.ResponseBase;


/**
 * Created by pqv on 26/11/15.
 */
public abstract class BaseDataManager {

    private MaxService maxApi;

    public BaseDataManager(Context context) {
        createMaxApi();
    }

    public abstract void onDataLoaded(ResponseBase data);

    public MaxService getMaxApi() {
        return maxApi;
    }

    private void createMaxApi() {
        maxApi = new Retrofit.Builder()
                .baseUrl(MaxService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MaxService.class);
    }
}
