package se.max.android.locator.utils;

/**
 * Created by pqv on 07/12/15.
 */
public class ReloadManager {

    private static final int UNDEFINED    = -1;
    public static final int AVAILABLE    =  0;
    public static final int UNAVAILABLE  =  1;
    private int prev;
    private int curr;

    public ReloadManager() {
        prev = UNDEFINED;
    }

    public void setCurrent(int curr) {
        this.curr = curr;
    }

    public boolean reload() {
        if (prev == UNDEFINED) {
            prev = curr;
            return true;
        } else if (prev != curr) {
            prev = curr;
            return true;
        } else {
            prev = curr;
            return false;
        }
    }
}
