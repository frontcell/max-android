package se.max.android.locator.ui.express;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import se.max.android.locator.R;
import se.max.android.locator.data.api.max.model.menu.MenuEntry;

public class MenuAdapter extends BaseExpandableListAdapter {

    private List<MenuEntry> mData;

    public MenuAdapter(List<MenuEntry> entries) {
        mData = entries;
    }

    @Override
    public Object getChild(int groupPos, int childPos) {
        MenuEntry child = null;
        if (mData != null) {
            child = mData.get(groupPos).getChildAt(childPos);
        }
        return child;
    }

    @Override
    public long getChildId(int groupPos, int childPos) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
            ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = View.inflate(parent.getContext(), R.layout.menu_child_item, null);
        }
        MenuEntry groupItem = mData.get(groupPosition).getChildAt(childPosition);

        TextView label = (TextView) v.findViewById(R.id.Label);
        label.setText(groupItem.getTitle());
        return v;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        int childCount = 0;
        if (mData != null && mData.size() > 0) {
            childCount = mData.get(groupPosition).getChildCount();
        }
        return childCount;
    }

    @Override
    public Object getGroup(int groupPosition) {
        MenuEntry group = null;
        if (mData != null && mData.size() > 0) {
            group = mData.get(groupPosition);
        }
        return group;
    }

    @Override
    public int getGroupCount() {
        int count = 0;
        if (mData != null) {
            count = mData.size();
        }
        return count;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            v = View.inflate(parent.getContext(), R.layout.menu_group_layout, null);
        }
        MenuEntry groupItem = mData.get(groupPosition);
        ImageView groupIndicator = ((ImageView) v.findViewById(R.id.GroupIndicator));

        TextView label = (TextView) v.findViewById(R.id.Label);
        label.setText(groupItem.getTitle());
        if (groupItem.getChildCount() > 0) {
            groupIndicator
                    .setImageResource(isExpanded ? R.drawable.ic_menu_close : R.drawable.ic_menu_expand);
            groupIndicator.setVisibility(View.VISIBLE);
        } else {
            groupIndicator.setVisibility(View.INVISIBLE);
        }

        return v;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
