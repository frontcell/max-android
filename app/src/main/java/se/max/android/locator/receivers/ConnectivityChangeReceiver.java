package se.max.android.locator.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by pqv on 27/11/15.
 */
public class ConnectivityChangeReceiver extends BroadcastReceiver {

    public interface ConnectivityInteractions {
        void onNetworkAvailable();
        void onNetworkUnAvailable();
    }

    private boolean isConnected;
    private ConnectivityInteractions listener;


    public ConnectivityChangeReceiver(Context context, ConnectivityInteractions listener) {
        this.listener = listener;
        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        ConnectivityManager cm =
                (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean active = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if (active != isConnected) {
            if (active) {
                listener.onNetworkAvailable();
            } else {
                listener.onNetworkUnAvailable();
            }
            isConnected = active;
        }
    }
}
