package se.max.android.locator.data.api.max;


import retrofit2.Call;
import retrofit2.http.GET;
import se.max.android.locator.data.api.max.model.location.LocationResponse;
import se.max.android.locator.data.api.max.model.menu.MenuResponse;

/**
 * Created by pqv on 26/11/15.
 */
public interface MaxService {

    String BASE_URL = "http://apps.max.se/max_app_7/sv/Feed/";

    @GET("Restaurants")
    Call<LocationResponse> getRestaurants();

    @GET("Navigation")
    Call<MenuResponse> getMenu();

}
