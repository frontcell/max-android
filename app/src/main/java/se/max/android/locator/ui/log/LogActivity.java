package se.max.android.locator.ui.log;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import se.max.android.locator.R;
import se.max.android.locator.ui.GlobalState;

/**
 * Created by Vu Phan on 2017-10-18.
 */

public class LogActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);

        TextView logTextView = (TextView) findViewById(R.id.logTextView);

        logTextView.setText(GlobalState.getLogText());
    }
}
