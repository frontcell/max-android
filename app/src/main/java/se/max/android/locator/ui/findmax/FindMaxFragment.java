package se.max.android.locator.ui.findmax;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.ViewSwitcher;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import se.max.android.locator.R;
import se.max.android.locator.analytics.AnalyticsWrapper;
import se.max.android.locator.analytics.AnalyticsWrapper.PageView;
import se.max.android.locator.data.DataManager;
import se.max.android.locator.data.api.max.model.ResponseBase;
import se.max.android.locator.data.api.max.model.location.LocationResponse;
import se.max.android.locator.data.api.max.model.location.MLocation;
import se.max.android.locator.data.api.max.model.menu.MenuResponse;
import se.max.android.locator.data.api.max.model.menu.RestaurantAttribute;
import se.max.android.locator.ui.BaseFragment;
import se.max.android.locator.ui.GlobalState;
import se.max.android.locator.utils.LocationUtils;
import se.max.android.locator.utils.Logger;
import se.max.android.locator.utils.MLocationComparators;


public class FindMaxFragment extends BaseFragment implements OnMyLocationChangeListener, OnItemSelectedListener, OnMapReadyCallback {

    private static final String NAVIGATION_DATA = "NavigationData";
    private static final float RECALCULATE_DISTANCES_THRESHOLD = 100f;
    private GoogleMap mMap; // the actual map
    private MapView mMapView; // we need this reference to feed it the fragment states
    private ViewSwitcher mSwitcher;
    private RadioGroup mTabs;
    private ListView mListView;
    private FindMaxAdapter mLocationAdapter;
    private Spinner mCountrySpinner;
    private ArrayAdapter<String> mCountryAdapter;
    private Spinner mRestaurantAttributeSpinner;
    private ArrayAdapter<String> mRestaurantAttributeAdapter;
    private Location mUserLocation = null;
    private FindMaxFragmentInteractions mListener;
    private LocationResponse mResturantLocations;
    private RadioGroup mSortByRadioGroup;

    private Map<String, RestaurantAttribute> mAttributeMap;
    private DataManager mDataManagar;
    private View mRootView;

    public static FindMaxFragment newInstance(String menuJson) {
        FindMaxFragment f = new FindMaxFragment();
        Bundle args = new Bundle();
        // add any arguments to the bundle
        args.putString(FindMaxFragment.NAVIGATION_DATA, menuJson);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Logger.d("FindMaxFragment onMapReady");
            mMap = googleMap;
            setUpMap();
            populateList();
    }

    public interface FindMaxFragmentInteractions {
        void onError(se.max.android.locator.data.api.max.model.Error error, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative);
        void onLocationClicked(String locationUrl);
    }



    private void setUpMapIfNeeded(MapView mapView) {

        Logger.d("FindMaxFragment setUpMapIfNeeded");
        MapsInitializer.initialize(getActivity());
        mapView.getMapAsync(this);

    }

    private void setUpMap() {
        int top = getActivity().getResources().getDimensionPixelSize(R.dimen.nintysix_dp);
        int right = getActivity().getResources().getDimensionPixelSize(R.dimen.eight_dp);
        mMap.setPadding(0, top, right, 0);
        // Check if permission is granted.
        int permissionCheckOK = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionCheckOK == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }
        mMap.setOnMyLocationChangeListener(this);
        mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

            @Override
            public void onInfoWindowClick(Marker marker) {
                if (mResturantLocations != null) {
                    String urlForMarker = mResturantLocations.getUrlForMarker(marker.getTitle(),
                            marker.getSnippet());
                    if (urlForMarker != null && mListener != null) {
                        mListener.onLocationClicked(urlForMarker);
                    }
                }
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        Logger.d("FindMaxFragment: onAttach");
        try {
            mListener = (FindMaxFragmentInteractions) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement FindMaxFragmentInteractions");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        if (mMap != null) {
            // subscribe to location events
            int permissionCheckOK = ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheckOK == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        if (mMap != null) {
            // dont subscribe to location events anymore
            int permissionCheckOK = ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheckOK == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(false);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Logger.d("FindMaxFragment:  onActivityCreated(...)");
        super.onActivityCreated(savedInstanceState);

        mDataManagar.loadRestaurants();
    }

    private void populateList() {
        Logger.d("FindMaxFragment populateList()");
        final List<MLocation> locations = mResturantLocations.getLocations();
        if (locations != null && locations.size() > 0) {
            if (mUserLocation != null) {
                for (MLocation location : locations) {
                    float distanceBetween = LocationUtils.distanceBetween(mUserLocation, location);
                    location.setDistanceToUser(distanceBetween);
                }
                Collections.sort(locations);
            }

            populateSpinnerAdapter(mCountryAdapter, getCountries(locations));
            mCountrySpinner.setAdapter(mCountryAdapter);
            mCountrySpinner.setSelection(0);

            populateSortByAttributeSpinnerAdapter();
            mRestaurantAttributeSpinner.setAdapter(mRestaurantAttributeAdapter);
            mRestaurantAttributeSpinner.setSelection(0);


            mMap.clear();
            for (MLocation l : locations) {
                MarkerOptions marker = l.createMarker();
                mMap.addMarker(marker);
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDataManagar = new DataManager(getActivity()) {
            @Override
            public void onDataLoaded(ResponseBase data) {
                if (data == null || data.hasError()) {
                    mListener.onError(data == null ? null : data.getError(), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mDataManagar.loadRestaurants();
                        }
                    }, null);

                    return;
                }
                mResturantLocations = (LocationResponse)data;

                Logger.d("FindMaxFragment onDataLoaded()");
                setUpMapIfNeeded(mMapView);
            }
        };

        mCountryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.express_simple_spinner_dropdown_item);
        mCountryAdapter.setDropDownViewResource(R.layout.express_simple_spinner_dropdown_item);
        mCountryAdapter.setNotifyOnChange(true);

        mRestaurantAttributeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.express_simple_spinner_dropdown_item);
        mRestaurantAttributeAdapter.setDropDownViewResource(R.layout.express_simple_spinner_dropdown_item);
        mRestaurantAttributeAdapter.setNotifyOnChange(true);

        AnalyticsWrapper.trackPageView(getActivity(), PageView.PAGEVIEW_FIND_MAX);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.d("FindMaxFragment onCreateView()");

        container.setPadding(0, 0, 0, 0);

        if (mRootView == null) {
            mRootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_findmax, container, false);


            ImageButton close = (ImageButton) mRootView.findViewById(R.id.close_button);
            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getActivity().finish();
                }
            });

            mMapView = (MapView) mRootView.findViewById(R.id.Map);
            mMapView.onCreate(savedInstanceState);


            mSwitcher = (ViewSwitcher) mRootView.findViewById(R.id.Switcher);
            mTabs = (RadioGroup) mRootView.findViewById(R.id.rg_nav);

            mListView = (ListView) mRootView.findViewById(R.id.LocationList);
            View header = LayoutInflater.from(getActivity()).inflate(R.layout.divider, null, false);
            mListView.addHeaderView(header);
            mListView.setEmptyView(mRootView.findViewById(android.R.id.empty));
            mLocationAdapter = new FindMaxAdapter();
            mListView.setAdapter(mLocationAdapter);
            mListView.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View v, int pos, long id) {
                    MLocation location = (MLocation) v.getTag();
                    mListener.onLocationClicked(location.getUrl());

                }
            });
            mTabs.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // The reason this works is that we only ever have 2 views in the switcher
                    switch (checkedId) {
                        case R.id.map_tab:
                            mSwitcher.showNext();
                            break;
                        case R.id.list_tab:
                            mSwitcher.showNext();
                            break;
                    }

                }
            });

            mCountrySpinner = (Spinner) mRootView.findViewById(R.id.CountrySpinner);
            mCountrySpinner.setOnItemSelectedListener(this);
            mSortByRadioGroup = (RadioGroup) mRootView.findViewById(R.id.rg_sort);

            mRestaurantAttributeSpinner = (Spinner) mRootView.findViewById(R.id.RestaurantAttributeSpinner);
            mRestaurantAttributeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    Logger.d("SortByAttributeSpinner item pos: " + position + " selected");
                    String attributeName = mRestaurantAttributeAdapter.getItem(position);

                    mLocationAdapter.applyFilter(mAttributeMap.get(attributeName));
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            mSortByRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {

                    if (checkedId == R.id.PositionRadioButton) {
                        mLocationAdapter.sortBy(MLocationComparators.BY_DISTANCE);

                    }
                    if (checkedId == R.id.AppNameRadioButton) {
                        mLocationAdapter.sortBy(MLocationComparators.BY_CITY_APP_NAME_COMBINATION);

                    }

                }

            });
        }
        return mRootView;
    }


    private void populateSpinnerAdapter(ArrayAdapter<String> adapter, List<String> countries) {
        adapter.clear();
        for (String country : countries) {
            adapter.add(country);
        }
    }

    private void populateSortByAttributeSpinnerAdapter() {
        String menuJson = getArguments().getString(FindMaxFragment.NAVIGATION_DATA);
        mAttributeMap = new HashMap<String, RestaurantAttribute>();
        if (menuJson == null) {
            // Nop.
        } else {
            final MenuResponse menuData = new Gson().fromJson(menuJson, MenuResponse.class);
            menuData.getRestaurantAttributes().add(0, new RestaurantAttribute("all", getString(R.string.restaurant_attribute_all)));
            for (RestaurantAttribute attribute : menuData.getRestaurantAttributes()) {
                mRestaurantAttributeAdapter.add(attribute.getName());
                mAttributeMap.put(attribute.getName(), attribute);
            }
        }
    }

    private List<String> getCountries(List<MLocation> locations) {

        // Temporarily solution
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Sweden", "Sverige");
        map.put("Norway", "Norge");
        map.put("Denmark", "Danmark");
        map.put("UAE", "Förenade Arabemiraten");

        ArrayList<String> countries = new ArrayList<String>();
        
        for (MLocation location : locations) {
            if (map.get(location.getCountry()) != null) {
                if (!countries.contains(map.get(location.getCountry()))) {
                    countries.add(map.get(location.getCountry()));
                }
            }

        }

        return countries;
    }

    private List<MLocation> getLocationsInCountry(String country) {
        ArrayList<MLocation> result = new ArrayList<MLocation>();

        for (MLocation location : mResturantLocations.getLocations()) {
            if (location.getCountry().equals(country)) {
                result.add(location);
            }
        }

        return result;
    }


    @Override
    public void onMyLocationChange(Location location) {
        if (location != null) {
            if (Logger.ENABLED) {
                Logger.d("Location Accuracy: " + location.getAccuracy());
                Logger.d("Location Lat: " + location.getLatitude());
                Logger.d("Location Lon: " + location.getLongitude());
                Logger.d("Location Provider: " + location.getProvider());
            }

            GlobalState.appendLogText("Location: onMyLocationChange " + location.toString() + "\n" + new Date(location.getTime()).toString());
        }

        if (mUserLocation == null) {
            mUserLocation = location;
            mLocationAdapter.setUserPosition(mUserLocation);
            // animate to user position:
            CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(new CameraPosition(new LatLng(
                    mUserLocation.getLatitude(), mUserLocation.getLongitude()), 12, 0f, 0f));
            mMap.animateCamera(cameraUpdate);
        } else {
            boolean recalculateAdapter = false;
            final float distanceBetween = LocationUtils.distanceBetween(location, mUserLocation);
            if (distanceBetween > RECALCULATE_DISTANCES_THRESHOLD) {
                recalculateAdapter = true;
                Logger.i("Distance between old position and new position was greater then the threshold value ("
                        + distanceBetween + ")");
            } else {
                Logger.i("Distance between old position and new position was less then the threshold value ("
                        + distanceBetween + ")");
            }
            mUserLocation = location;

            if (recalculateAdapter) {
                mLocationAdapter.setUserPosition(mUserLocation);
            }

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Logger.d("Spinner item pos: " + position + "selected");

        // Temporarily solution
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Sverige", "Sweden");
        map.put("Norge", "Norway");
        map.put("Danmark", "Denmark");
        map.put("Förenade Arabemiraten", "UAE");

        switch (position) {
        default:
            String country = mCountryAdapter.getItem(position);
            mLocationAdapter.setData(getLocationsInCountry(map.get(country)));
            break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // TODO Auto-generated method stub

    }

}
