package se.max.android.locator.data.api.max.model.location;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import se.max.android.locator.data.api.max.model.ResponseBase;

public class LocationResponse extends ResponseBase {
    @SerializedName("restaurantList")
    private List<MLocation> mLocations = null;

    public List<MLocation> getLocations() {
        return mLocations;
    }

    @Override
    public String toString() {
        return "LocationResponse [mLocations=" + mLocations + ", mError=" + mError + "]";
    }

    public String getUrlForMarker(String name, String address) {
        String url = null;
        if (mLocations != null) {
            for (MLocation location : mLocations) {
                if (location.getName().equals(name) && location.getAddress().equals(address)) {
                    url = location.getUrl();
                    break;
                }
            }
        }

        return url;
    }
}
