package se.max.android.locator.data.api.max.model;

import com.google.gson.annotations.SerializedName;

public class ResponseBase {

    @SerializedName("maxError")
    protected Error mError = null;

    public boolean hasError() {
        return mError != null;
    }

    public Error getError() {
        return mError;
    }
}
