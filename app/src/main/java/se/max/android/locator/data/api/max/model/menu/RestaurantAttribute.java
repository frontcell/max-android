package se.max.android.locator.data.api.max.model.menu;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pqv on 04/05/15.
 */
public class RestaurantAttribute {

    @SerializedName("id")
    private String mId = null;

    @SerializedName("name")
    private String mName = null;

    public String getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public RestaurantAttribute(String id, String name) {
        mId = id;
        mName = name;
    }

    @Override public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof RestaurantAttribute) {
            RestaurantAttribute that = (RestaurantAttribute) other;
            result = this.getId().equals(that.getId());
        }
        return result;
    }

    @Override public int hashCode() {
        return getId().hashCode();
    }
}
