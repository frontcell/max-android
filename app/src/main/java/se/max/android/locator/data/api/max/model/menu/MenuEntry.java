package se.max.android.locator.data.api.max.model.menu;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MenuEntry {

    @SerializedName("heading")
    private String mTitle = null;

    @SerializedName("url")
    private String mUrl = null;

    @SerializedName("guid")
    private String mGuid = null;

    @SerializedName("children")
    private List<MenuEntry> mChildren = null;

    public String getTitle() {
        return mTitle;
    }

    public String getUrl() {
        return mUrl;
    }

    public MenuEntry getChildAt(int pos) {
        MenuEntry child = null;
        if (getChildren() != null) {
            if (pos < getChildren().size()) {
                child = getChildren().get(pos);
            }
        }
        return child;
    }

    public List<MenuEntry> getChildren() {
        if (mChildren == null) {
            mChildren = new ArrayList<>();
        }
        return mChildren;
    }

    public int getChildCount() {
        int childCount = 0;
        if (getChildren() != null) {
            childCount = getChildren().size();
        }
        return childCount;
    }

    public boolean isLeaf() {
        return getChildCount() == 0;
    }

    @Override
    public String toString() {
        return "MenuEntry [mTitle=" + getTitle() + ", mUrl=" + getUrl() + ", mChildren=" + getChildren()
                + "]";
    }

    public String getId() {
        return getGuid();
    }

    public void setTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    public String getGuid() {
        return mGuid;
    }

    public void setGuid(String mGuid) {
        this.mGuid = mGuid;
    }

    public void setChildren(List<MenuEntry> mChildren) {
        this.mChildren = mChildren;
    }

}
