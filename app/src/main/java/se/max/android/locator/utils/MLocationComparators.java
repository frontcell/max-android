package se.max.android.locator.utils;

import java.util.Comparator;

import se.max.android.locator.data.api.max.model.location.MLocation;


public class MLocationComparators {

    public static Comparator<MLocation> BY_DISTANCE = new Comparator<MLocation>() {

        @Override
        public int compare(MLocation lhs, MLocation rhs) {
            if (lhs.getDistanceToUser() < rhs.getDistanceToUser())
                return -1;
            if (lhs.getDistanceToUser() > rhs.getDistanceToUser())
                return +1;

            return 0;
        }

    };

    public static Comparator<MLocation> BY_NAME = new Comparator<MLocation>() {

        @Override
        public int compare(MLocation lhs, MLocation rhs) {
            return lhs.getName().compareTo(rhs.getName());
        }

    };
    
    public static Comparator<MLocation> BY_CITY = new Comparator<MLocation>() {

        @Override
        public int compare(MLocation lhs, MLocation rhs) {
            
            return lhs.getCity().compareTo(rhs.getCity());
        }
 
    };
    
    public static Comparator<MLocation> BY_APP_NAME = new Comparator<MLocation>() {
        
        @Override
        public int compare(MLocation lhs, MLocation rhs) {
            return lhs.getAppName().compareTo(rhs.getAppName());
        }
    };
    
    public static Comparator<MLocation> BY_CITY_APP_NAME_COMBINATION = new Comparator<MLocation>() {

        @Override
        public int compare(MLocation lhs, MLocation rhs) {
            return (lhs.getCity() + lhs.getAppName()).compareTo(rhs.getCity()+ rhs.getAppName());
        }
        
    };
}
