package se.max.android.locator.data.api.max.model.menu;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pqv on 10/05/16.
 */
public class ReferenceMenuResponse extends MenuResponse {

    private boolean injected;

    /**
     * Returns the root of the tree. The tree is always depth 3 if not null
     */
    @Override
    public MenuEntry getRoot() {
        if (!injected) {
            inject(super.getRoot());
        }

        return super.getRoot();
    }

    private void inject(MenuEntry menu) {
        injected = true;
        MenuEntry newRoot = new MenuEntry();
        newRoot.setTitle("Inställningar");
        newRoot.setUrl("maxapp://settings/");
        MenuEntry oldRoot = new MenuEntry();
        oldRoot.setTitle(menu.getTitle());
        oldRoot.setGuid(menu.getGuid());
        oldRoot.setUrl(menu.getUrl());
        List<MenuEntry> list = new ArrayList<>();
        list.add(oldRoot);
        list.addAll(menu.getChildren());
        newRoot.setChildren(list);

        setRoot(newRoot);
    }
}
