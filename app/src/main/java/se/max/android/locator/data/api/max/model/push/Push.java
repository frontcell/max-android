package se.max.android.locator.data.api.max.model.push;

import com.google.gson.annotations.SerializedName;

/**
 * Created by pqv on 07/12/15.
 */
public class Push {

    @SerializedName("view")
    private String view;

    @SerializedName("url")
    private String url;

    @SerializedName("msg")
    private String msg;

    @SerializedName("id")
    private String id;

    public Push(String view, String url, String msg) {
        this.id = Long.toString(System.currentTimeMillis());
        this.view = view != null ? view : "";
        this.url = url != null ? url : "";
        this.msg = msg != null ? msg : "";
    }

    public String getView() {
        return view;
    }

    public String getUrl() {
        return url;
    }

    public String getMsg() {
        return msg;
    }

    public String getId() {
        return id;
    }


    @Override
    public String toString() {
        return "id: " + id + " view: " + view + " url: " + url + " msg: " + msg;
    }
}
