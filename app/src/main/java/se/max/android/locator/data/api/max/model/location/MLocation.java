package se.max.android.locator.data.api.max.model.location;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import se.max.android.locator.R;
import se.max.android.locator.data.api.max.model.menu.RestaurantAttribute;

public class MLocation implements Comparable<MLocation> {
    @SerializedName("guid")
    private String mGuid = null;

    @SerializedName("name")
    private String mName = null;

    @SerializedName("url")
    private String mUrl = null;

    @SerializedName("latitude")
    private float mLat = 0.0f;

    @SerializedName("longitude")
    private float mLon = 0.0f;

    @SerializedName("address")
    private String mAddress = null;

    @SerializedName("city")
    private String mCity = null;

    @SerializedName("country")
    private String mCountry = null;
    
    @SerializedName("appname")
    private String mAppName = null;

    @SerializedName("attributes")
    private ArrayList<RestaurantAttribute> mAttributes = null;

    private float mDistanceToUser = Float.MAX_VALUE;

    /**
     * Latitude of location
     * 
     * @return
     */
    public float getLat() {
        return mLat;
    }

    /**
     * Longitude of location
     * 
     * @return
     */
    public float getLon() {
        return mLon;
    }

    /**
     * Get the unique id of this location
     * 
     * @return
     */
    public String getGuid() {
        return mGuid;
    }

    /**
     * Get the name of location
     * 
     * @return
     */
    public String getName() {
        return mName;
    }

    /**
     * Get url to the info page of this location
     * 
     * @return
     */
    public String getUrl() {
        return mUrl;
    }

    /**
     * Get the address for this location
     * 
     * @return
     */
    public String getAddress() {
        return mAddress;
    }

    /**
     * Get the city for this location
     * 
     * @return
     */
    public String getCity() {
        return mCity;
    }

    /**
     * Get the country for this location
     * 
     */
    public String getCountry() {
        return mCountry;
    }
    
    /**
     * Get the app name for this location
     * @return
     */
    public String getAppName() {
        return mAppName;
    }

    public ArrayList<RestaurantAttribute> getAttributes() {
        return mAttributes;
    }

    /**
     * Creates a map marker. Note that each call creates a new map marker object.
     * 
     * @return
     */
    public MarkerOptions createMarker() {
        final MarkerOptions marker = new MarkerOptions();
        marker.title(mName);
        marker.icon(BitmapDescriptorFactory.fromResource(R.drawable.max_logo_map));
        marker.snippet(mAddress);
        marker.position(new LatLng(mLat, mLon));
        return marker;
    }

    @Override
    public String toString() {
        return "Location [mGuid=" + mGuid + ", mName=" + mName + ", mImageUrl=" + mUrl + ", mLat=" + mLat
                + ", mLon=" + mLon + ", mCity=" + mCity + ", mCountry=" + mCountry + "mAppName=" + mAppName + "]";
    }

    public void setDistanceToUser(float distanceToUser) {
        mDistanceToUser = distanceToUser;
    }

    /**
     * Returns the calculated distance from the location to the users current location. If the
     * distance hasn't been calculated the distance returned will be Float.MAX_VALUE
     * 
     * @return The distance (in meters) from the user to the location if set, otherwise
     *         Float.MAX_VALUE
     */
    public float getDistanceToUser() {
        return mDistanceToUser;
    }

    public String getDistanceString() {
        StringBuilder builder = new StringBuilder();
        if (mDistanceToUser == Float.MAX_VALUE) {
            builder.append("-");
        } else {
            if (mDistanceToUser < 1000f) {
                builder.append(Math.round(mDistanceToUser));
                builder.append(" m");
            } else {
                builder.append(Math.round(mDistanceToUser / 1000f));
                builder.append(" km");
            }
        }
        return builder.toString();
    }

    @Override
    public int compareTo(MLocation another) {
        return Float.valueOf(mDistanceToUser).compareTo(Float.valueOf(another.mDistanceToUser));
    }

}
