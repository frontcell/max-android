package se.max.android.locator.utils;


import android.location.Location;

import se.max.android.locator.data.api.max.model.location.MLocation;

public class LocationUtils {
    private LocationUtils() {
        // no instances!
    }

    public static float distanceBetween(Location l, MLocation ml) {
        float result = 0;
        if (l == null || ml == null) {
            result = -1;
        } else {
            final float[] results = new float[3];
            Location.distanceBetween(l.getLatitude(), l.getLongitude(), ml.getLat(), ml.getLon(), results);
            result = results[0];
        }
        return result;
    }

    public static float distanceBetween(Location newLocation, Location oldLocation) {
        float result = 0;
        if (newLocation == null || oldLocation == null) {
            result = -1;
        } else {
            final float[] results = new float[3];
            Location.distanceBetween(newLocation.getLatitude(), newLocation.getLongitude(),
                    oldLocation.getLatitude(), oldLocation.getLongitude(), results);
            result = results[0];
        }
        return result;
    }
}
