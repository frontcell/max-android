package se.max.android.locator.utils;

import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.widget.Toast;

import se.max.android.locator.R;

public class RatingManager {

    private static final String PREFS_RATING = "prefsRating";
    private static final String SETTING_RATING_HAS_BEEN_SHOWN = "ratingHasBeenShown";
    private static final String SETTING_EXPRESS_PAGEVIEW_COUNT = "expressPageviewCount";


    private final SharedPreferences.OnSharedPreferenceChangeListener mListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            if (key.equals(SETTING_EXPRESS_PAGEVIEW_COUNT)) {
                boolean hasBeenShown = sharedPreferences.getBoolean(SETTING_RATING_HAS_BEEN_SHOWN, false);
                int count = sharedPreferences.getInt(SETTING_EXPRESS_PAGEVIEW_COUNT, 0);
                if (!hasBeenShown && count > 10) {
                    showRatingDialog();
                }
            }
        }
    };

    private Context mContext;

    public RatingManager(Context context) {
        mContext = context;
        SharedPreferences prefs = context.getSharedPreferences(PREFS_RATING, Context.MODE_PRIVATE);
        prefs.registerOnSharedPreferenceChangeListener(mListener);
    }

    public void incExpressPageviewCount() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_RATING, Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        int old = getExpressPageViewCount();
        old++;
        editor.putInt(SETTING_EXPRESS_PAGEVIEW_COUNT, old);
        editor.commit();
    }

    private boolean getRatingsHasBeenShown() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_RATING, Context.MODE_PRIVATE);
        return prefs.getBoolean(SETTING_RATING_HAS_BEEN_SHOWN, false);
    }

    private void setRatingsHasBeenShown() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_RATING, Context.MODE_PRIVATE);
        Editor editor = prefs.edit();
        editor.putBoolean(SETTING_RATING_HAS_BEEN_SHOWN, true);
        editor.commit();
    }

    private int getExpressPageViewCount() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFS_RATING, Context.MODE_PRIVATE);
        return prefs.getInt(SETTING_EXPRESS_PAGEVIEW_COUNT, 0);
    }

    private void showRatingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(R.string.rating_dialog_title);
        builder.setMessage(R.string.rating_dialog_message);
        builder.setPositiveButton(R.string.rating_positive_button,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setRatingsHasBeenShown();
                        launchMarket(mContext);
                    }
                });
        builder.setNegativeButton(R.string.rating_negative_button,
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setRatingsHasBeenShown();
                    }
                });
        builder.create().show();
    }

    private void launchMarket(Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            context.startActivity(myAppLinkToMarket);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(context, R.string.rating_error_toast, Toast.LENGTH_SHORT).show();
        }
    }
}
