package se.max.android.locator.data.api.max.model.menu;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.data.api.max.model.ResponseBase;
import se.max.android.locator.ui.settings.PreferenceActivity;

public class MenuResponse extends ResponseBase {


    @SerializedName("startPage")
    private MenuEntry mMenuRoot = null;

    @SerializedName("AllRestaurantAttributes")
    private ArrayList<RestaurantAttribute> mRestaurantAttributes = null;

    @SerializedName("nativeStartUrl")
    private String mNativeStartUrl;

    /**
     * Returns the root of the tree. The tree is always depth 3 if not null
     */
    public MenuEntry getRoot() {
        return mMenuRoot;
    }

    public void setRoot(MenuEntry root) {
        mMenuRoot = root;
    }

    public ArrayList<RestaurantAttribute> getRestaurantAttributes() {
        return mRestaurantAttributes;
    }

    public String getNativeStartUrl(Context context) {
        switch (BuildConfig.NATIVE_URL_SOURCE) {

            case NATIVE:
                return mNativeStartUrl;
            case SHARED_PREFS:
                return getFromSharedPrefs(context);
            case BUILD_CONFIG:
                return BuildConfig.BASE_URL;
            default:
                return mNativeStartUrl;
        }
    }

    @Override
    public String toString() {
        return "MenuResponse [mMenuRoot=" + mMenuRoot + "]";
    }

    private String getFromSharedPrefs(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PreferenceActivity.PREFS_SETTINGS, Context.MODE_PRIVATE);
        int choice = prefs.getInt(PreferenceActivity.SETTING_CHOICE, PreferenceActivity.SETTING_CHOICE_DEFAULT);
        String customUrl = prefs.getString(PreferenceActivity.SETTING_CUSTOM_URL, "");

        switch (choice) {
            case 1: // choice = 1 // android:text="https://ref-ace-online.max.se/" />
                return "https://ref-ace-online.max.se/";
            case 2: // choice = 2 // android:text="https://test-ace-online.max.se/" />
                return "https://test-ace-online.max.se/";
            case 3: // choice = 3 // android:text="https://ace-online.max.se/" />
                return "https://ace-online.max.se/";
            case 4: // choice = 4 // android:text="native" />
                return mNativeStartUrl;
            case 5: // choice = 5 // android:text="custom" />
                return customUrl;
            default:
                return mNativeStartUrl;
        }
    }

}
