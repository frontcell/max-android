package se.max.android.locator.ui.express;

import android.os.Bundle;
import android.webkit.WebView;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.ui.WebFragment;
import se.max.android.locator.utils.Logger;

/**
 * Created by pqv on 04/12/15.
 */
public class ExpressWebFragment extends WebFragment {

    public static ExpressWebFragment newInstance() {
        Logger.d("ExpressWebFragment newInstance()");
        ExpressWebFragment f = new ExpressWebFragment();
        Bundle args = new Bundle();
        args.putString(URL_ARGS, BuildConfig.BASE_URL);
        f.setArguments(args);
        return f;
    }

    public static ExpressWebFragment newInstance(String url) {
        Logger.d("ExpressWebFragment newInstance() " + url);
        ExpressWebFragment f = new ExpressWebFragment();
        Bundle args = new Bundle();
        args.putString(URL_ARGS, url);
        f.setArguments(args);
        return f;
    }

    @Override
    public void setWebViewClient(WebView webView) {
        Logger.d("ExpressWebFragment setWebVewClient()");
        webView.setWebViewClient(new ExpressWebViewClient());
    }

    @Override
    public boolean onBackPress() {
        Logger.d("ExpressWebFragment onBackBress()");
        if (isBackNavigationDisabled()) {
            Logger.d("ExpressWebFragment BackNavigationDisabled");
            return true;
        } else {
            return super.onBackPress();
        }
    }




}
