package se.max.android.locator.analytics;

import android.content.Context;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import se.max.android.locator.BuildConfig;
import se.max.android.locator.application.MaxApp;
import se.max.android.locator.utils.Logger;

import static se.max.android.locator.utils.Common.notNull;


public class AnalyticsWrapper {

    public enum PageView {
        PAGEVIEW_FIND_MAX("/findmax"),
        NAVIGATON_MENU("/navigationmenu");

        private String path;

        PageView(String path) {
            this.path = path;
        }
    }

    public enum Event {
        REGISTER_PUSH_FAIL("PUSH", "push_register", "");
        private String category;
        private String action;
        private String label;
        

        Event(String category, String action, String label) {
            this.category = category;
            this.action = action;
            this.label = label;

        }
        
        public void setLabel(String rhs) {
            label = rhs;
        }
        
        @Override
        public String toString() {
            return category + " " + action + " " + label;
        }

    }

    private AnalyticsWrapper() {

    }

    public static String getAnalyticsKey() {
        return BuildConfig.ANALYTICS_KEY;
    }

    public static void trackPageView(Context context, PageView pageView) {
        notNull(pageView, "PageView is null");

        Logger.d("trackPageView " + pageView.toString());
        Tracker t = MaxApp.getMaxApp(context).getTracker();
        t.setScreenName(pageView.path);
        t.send(new HitBuilders.AppViewBuilder().build());

    }

    public static void trackEvent(Context context, Event event) {
        notNull(event, "Event is null");

        Tracker t = MaxApp.getMaxApp(context).getTracker();
        t.send(new HitBuilders.EventBuilder()
                .setCategory(event.category)
                .setAction(event.action)
                .setLabel(event.label)
                .build());
    }

}
