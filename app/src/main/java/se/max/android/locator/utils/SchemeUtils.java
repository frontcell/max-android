package se.max.android.locator.utils;

/**
 * Created by pqv on 02/12/15.
 */
public class SchemeUtils {

    public enum Scheme {
        MAX_APP("maxapp://"),
        MAX_EXPRESS("maxexpress://"),
        HTTP("http://"),
        HTTPS("https://"),
        MAILTO("mailto:"),
        BANKID("bankid://"),
        BANKID_NORDEA("intent:#Intent;scheme=bankid;package=com.bankid.bus;end;"),
        NETAXEPT("epayment.nets.eu");

        private String scheme;

        Scheme(String scheme) {
            this.scheme = scheme;
        }

        public String getScheme() {
            return scheme;
        }

    }

    public static Scheme getScheme(String url) {
        if (url.contains(Scheme.NETAXEPT.getScheme())) {
            return Scheme.NETAXEPT;
        } else if (url.startsWith(Scheme.MAX_APP.getScheme())) {
            return Scheme.MAX_APP;
        } else if (url.startsWith(Scheme.MAX_EXPRESS.getScheme())) {
            return Scheme.MAX_EXPRESS;
        } else if (url.startsWith(Scheme.HTTP.getScheme())) {
            return Scheme.HTTP;
        } else if (url.startsWith(Scheme.HTTPS.getScheme())) {
            return Scheme.HTTPS;
        } else if (url.startsWith(Scheme.MAILTO.getScheme())) {
            return Scheme.MAILTO;
        } else if (url.startsWith(Scheme.BANKID.getScheme())) {
            return Scheme.BANKID;
        } else if (url.startsWith(Scheme.BANKID_NORDEA.getScheme())) {
            return Scheme.BANKID_NORDEA;
        } else {
            return null;
        }
    }
}
