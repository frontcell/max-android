package se.max.android.locator.utils;

import android.util.Log;

import se.max.android.locator.BuildConfig;

public class Logger {
    public static final boolean ENABLED = BuildConfig.LOG_ENABLED;
    public static final String TAG = "MaxApp";

    private Logger() {
    }

    public static void d(String message) {
        if (ENABLED) {
            Log.d(TAG, message);
        }
    }

    public static void i(String message) {
        if (ENABLED) {
            Log.i(TAG, message);
        }
    }

    public static void e(String message) {
        if (ENABLED) {
            Log.e(TAG, message);
        }
    }

    public static void w(String message) {
        if (ENABLED) {
            Log.w(TAG, message);
        }
    }
}
