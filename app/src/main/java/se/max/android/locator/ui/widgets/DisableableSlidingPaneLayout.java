package se.max.android.locator.ui.widgets;

import android.content.Context;
import android.support.v4.widget.SlidingPaneLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by pqv on 02/12/15.
 */
public class DisableableSlidingPaneLayout extends SlidingPaneLayout{

    private boolean disabled;

    public DisableableSlidingPaneLayout(Context context) {
        super(context);
    }

    public DisableableSlidingPaneLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public DisableableSlidingPaneLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void enable() {
        disabled = false;
    }

    public void disable() {
        disabled = true;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return disabled || super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (disabled) {
            getChildAt(1).dispatchTouchEvent(ev);
            return true;
        }
        return super.onTouchEvent(ev);
    }

}
