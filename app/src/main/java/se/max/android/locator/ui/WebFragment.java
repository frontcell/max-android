package se.max.android.locator.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.MailTo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.facebook.AppEventsConstants;

import se.max.android.locator.R;
import se.max.android.locator.utils.Common;
import se.max.android.locator.utils.Logger;
import se.max.android.locator.utils.SchemeUtils;

public class WebFragment extends BaseFragment {

    public static final String URL_ARGS = "URL";

    private View mRootView;

    public interface WebFragmentInteractions {

        void onError(se.max.android.locator.data.api.max.model.Error error, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative);

        void onOpenUrl(String url);

        void onClosePage();

        void onCloseMaxPage();

        void onLogFacebookEvent(String event);

        void onOpenNavigation();

        void onProgressChanged(int progress);

        void onPageLoaded(String url);

        void onOpenSettings();

        void onVibrate();

        void onFindMax();

        void onPlaySound();

        String onAppendUrl(String url);

        void onBankIdMissing();

    }

    public static WebFragment newInstance(String url) {
        Logger.d("WebFragment newInstance() " + url);
        WebFragment f = new WebFragment();
        Bundle args = new Bundle();
        args.putString(URL_ARGS, url);
        f.setArguments(args);
        return f;
    }

    private WebView mWebView;
    private WebFragmentInteractions mListener;
    private boolean mBackNavigationDisabled;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof WebFragmentInteractions) {
            mListener = (WebFragmentInteractions) activity;
        }
    }

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Logger.d("WebFragment onCreateView()");
        if (mRootView == null) {

            mRootView = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_web, container, false);

            mWebView = (WebView) mRootView.findViewById(R.id.WebView);
            mWebView.setOverScrollMode(View.OVER_SCROLL_NEVER);
            WebView.setWebContentsDebuggingEnabled(true);
            WebSettings settings = mWebView.getSettings();
            settings.setDomStorageEnabled(true);
            settings.setDatabaseEnabled(true);
            settings.setAllowContentAccess(true);
            settings.setJavaScriptEnabled(true);
            mWebView.addJavascriptInterface(new MaxWebInterface(), "Android");

            Logger.d("WebFragment UA=" + settings.getUserAgentString());
            setWebViewClient(mWebView);
            setWebChromeClient(mWebView);

            Bundle arguments = getArguments();
            if (arguments != null) {
                String url = arguments.getString(URL_ARGS);
                if (url != null) {
                    if (mListener != null) {
                        String appendedUrl = mListener.onAppendUrl(url);
                        Logger.d("WebFragment loading url: " + appendedUrl);

                        mWebView.loadUrl(appendedUrl);
                    }
                }
            }
        }


        return mRootView;
    }

    public boolean isBackNavigationDisabled() {
        return mBackNavigationDisabled;
    }

    public void reload() {
        mWebView.loadUrl(mListener.onAppendUrl(mWebView.getUrl()));
    }

    public void load(String url) {
        mWebView.loadUrl(mListener.onAppendUrl(url));
    }

    public void setWebViewClient(WebView webView) {
        webView.setWebViewClient(new MaxWebViewClient());
    }

    public void setWebChromeClient(WebView webView) {
        webView.setWebChromeClient(new MaxWebChromeClient());
    }

    public void loadUrlAndClearHistory(String url) {
        mWebView.setTag(String.valueOf("CLEAR_HISTORY"));
        mWebView.loadUrl(url);
    }

    public void nullOutListener() {
        mListener = null;
    }

    @Override
    public boolean onBackPress() {
        Logger.d("WebFragment onBackPress()");
        if (mWebView.canGoBack()) {
            mWebView.goBack();
            return true;
        }

        return false;
    }

    private class MaxWebChromeClient extends WebChromeClient {
        private View mCustomView;
        private int mOriginalSystemUiVisibility;
        private int mOriginalOrientation;
        private CustomViewCallback mCustomViewCallback;

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            if (mListener != null) {
                mListener.onProgressChanged(newProgress);
            }

        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            Logger.d("WebFragment onShowCustomView()");

            if (mCustomView != null) {
                onHideCustomView();
                return;
            }

            mCustomView = view;
            mOriginalSystemUiVisibility = getActivity().getWindow().getDecorView().getSystemUiVisibility();
            mOriginalOrientation = getActivity().getRequestedOrientation();

            mCustomViewCallback = callback;

            FrameLayout decor = (FrameLayout) getActivity().getWindow().getDecorView();
            decor.addView(mCustomView, new FrameLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));


            getActivity().getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE);

            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        @Override
        public void onHideCustomView() {
            Logger.d("WebFragment onHideCustomView()");
            FrameLayout decor = (FrameLayout) getActivity().getWindow().getDecorView();
            decor.removeView(mCustomView);
            mCustomView = null;

            getActivity().getWindow().getDecorView().setSystemUiVisibility(mOriginalSystemUiVisibility);
            getActivity().setRequestedOrientation(mOriginalOrientation);

            mCustomViewCallback.onCustomViewHidden();
            mCustomViewCallback = null;
        }
    }

    public class ExpressWebViewClient extends MaxWebViewClient {

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                Logger.d("ExpressWebViewClient onReceivedError " + request.getUrl() + " " + error.getErrorCode() + " " + error.getDescription());
            }

        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, final String url) {
            Logger.d("ExpressWebViewClient shouldOverrideUrlLoading() " + url);
            if (!Common.isNetworkAvailable(getContext())) {
                Logger.d("ExpressWebViewClient" + "shouldOverrideUrlLoading() network not available");
                return true;
            }

            if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.MAX_APP) {
                mListener.onOpenUrl(url);

            } else if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.MAILTO) {
                MailTo mailTo = MailTo.parse(url);
                String to = mailTo.getTo();
                if (to == null) {
                    return true;
                }

                String[] addresses = to.split(",");

                Intent intent = Common.createEmailIntent(addresses);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }
                return true;
            }
//            else if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.NETAXEPT) {
//                ((BaseActivity) getActivity()).showDialog(R.string.open_external_browser_title, R.string.do_payment_alert, R.string.pay, R.string.cancel, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        Intent intent = Common.createNetAxeptIntent(url);
//                        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
//                            startActivity(intent);
//                        }
//                    }
//                }, null, false);
//            }
            else if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.BANKID) {
                Intent intent = Common.createBankIDIntent(url);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    mListener.onBankIdMissing();
                }
                return true;
            } else if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.BANKID_NORDEA) {
                Intent intent = Common.createBankIDIntent("bankid:///?redirect=null");
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    mListener.onBankIdMissing();
                }
                return true;
            }
            return false;
        }
    }

    class MaxWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Logger.d("MaxWebViewClient shouldOverrideUrlLoading() " + url);
            if (!Common.isNetworkAvailable(getContext())) {
                return true;
            }
            if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.MAX_APP) {
                mListener.onOpenUrl(url);
            } else if (SchemeUtils.getScheme(url) == SchemeUtils.Scheme.MAILTO) {
                MailTo mailTo = MailTo.parse(url);
                String to = mailTo.getTo();
                if (to == null) {
                    return true;
                }

                String[] addresses = to.split(",");

                Intent intent = Common.createEmailIntent(addresses);
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }
            } else if (url.contains("maps.google.com")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }
            } else if (!url.contains(".max.se")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(intent);
                }
            } else {
                view.loadUrl(url);
            }
            return false;
        }


        @Override
        public void onPageFinished(WebView view, String url) {
            Logger.d("MaxWebViewClient onPageFinished() " + url);
            super.onPageFinished(view, url);
            if (view.getTag() != null) {
                mWebView.clearHistory();
                mWebView.setTag(null);
                Logger.i("WebView history cleared");
            }

            if (mListener != null) {
                mListener.onPageLoaded(url);
            }
        }
    }


    public class MaxWebInterface {

        /**
         * Handle a message from the web page.
         *
         * @param message
         */
        @JavascriptInterface
        public void sendMessage(String message) {
            if (message.equals("close")) {
                mListener.onClosePage();
            } else if (message.equals("closemax")) {
                mListener.onCloseMaxPage();
            } else if (message.equals("findmax")) {
                mListener.onFindMax();
            } else if (message.equals("playsound")) {
                mListener.onPlaySound();
            } else if (message.equals("vibrate")) {
                mListener.onVibrate();
            } else if (message.equals("opennav")) {
                mListener.onOpenNavigation();
            } else if (message.equals("opensettings")) {
                mListener.onOpenSettings();
            } else if (message.equals("purchase")) {
                mListener.onLogFacebookEvent(AppEventsConstants.EVENT_NAME_PURCHASED);
            } else if (message.equals("start order")) {
                mListener.onLogFacebookEvent(AppEventsConstants.EVENT_NAME_VIEWED_CONTENT);
            } else if (message.equals("add payment")) {
                mListener.onLogFacebookEvent(AppEventsConstants.EVENT_NAME_ADDED_PAYMENT_INFO);
            } else if (message.equals("register account")) {
                mListener.onLogFacebookEvent(AppEventsConstants.EVENT_NAME_COMPLETED_REGISTRATION);
            } else if (message.equals("disable backbutton")) {
                Logger.d("WebFragment message " + message);
                mBackNavigationDisabled = true;
            } else if (message.equals("enable backbutton")) {
                Logger.d("WebFragment message " + message);
                mBackNavigationDisabled = false;
            } else {
                Logger.d("WebFragment message " + message + " from web page not handled!");
            }
        }
    }
}
