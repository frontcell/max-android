package se.max.android.locator.ui;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.adform.adformtrackingsdk.AdformTrackingSdk;
import com.facebook.AppEventsLogger;

import java.util.Date;
import java.util.Stack;

import se.max.android.locator.R;
import se.max.android.locator.receivers.ConnectivityChangeReceiver;
import se.max.android.locator.utils.Logger;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by pqv on 26/11/15.
 */
public class BaseActivity extends AppCompatActivity implements ConnectivityChangeReceiver.ConnectivityInteractions, WebFragment.WebFragmentInteractions {

    private static final int LOCATION_REQUEST = 0x01;
    private static final int NETWORK_LOCATION_ACCURACY_THRESHOLD = 10000; // in meters.
    private static final int GPS_LOCATION_ACCURACY_THRESHOLD = 3000;
    private static final int DEFAULT_LOCATION_ACCURACY_THRESHOLD = 5000;

    private Stack<String> mFragmentTagNameStack;


    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location l) {
            Logger.d("Location: onLocationChanged( + " + l.toString() + ") " + l.getProvider());
            GlobalState.appendLogText("Location: onLocationChanged " + l.toString() + "\n" + new Date(l.getTime()).toString());
            int locationAccuracyThreshold;
            switch (l.getProvider()) {
                case "gps":
                    locationAccuracyThreshold = GPS_LOCATION_ACCURACY_THRESHOLD;
                    break;
                case "network":
                    locationAccuracyThreshold = NETWORK_LOCATION_ACCURACY_THRESHOLD;
                    break;
                default:
                    locationAccuracyThreshold = DEFAULT_LOCATION_ACCURACY_THRESHOLD;
            }

            if (l.getAccuracy() <= locationAccuracyThreshold) {
                setLocation(l);
                Logger.d("Location: " + l.getAccuracy() + " <= " + locationAccuracyThreshold + " passed.");
                GlobalState.appendLogText("Location: " + l.getAccuracy() + " <= " + locationAccuracyThreshold + " passed.");
                Logger.d("Location: Stop listening for location updates.");
                GlobalState.appendLogText("Location: Stop listening for location updates.");
                // Acquire a reference to the system Location Manager.
                LocationManager locationManager = (LocationManager) BaseActivity.this.getSystemService(Context.LOCATION_SERVICE);
                locationManager.removeUpdates(this);
                if (dialog != null && dialog.isShowing()) {
                    dialog.dismiss();
                    dialog = null;
                }
            } else {
                Logger.d("Location: " + l.getAccuracy() + " <= " + locationAccuracyThreshold + " not passed.");
                GlobalState.appendLogText("Location: " + l.getAccuracy() + " <= " + locationAccuracyThreshold + " not passed.");
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    private Location location;
    private ConnectivityChangeReceiver receiver;
    private Snackbar snackbar;
    private AlertDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFragmentTagNameStack = new Stack<>();
        receiver = new ConnectivityChangeReceiver(this, this);


    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Facebook event logging.
        AppEventsLogger.activateApp(this, getResources().getString(R.string.app_id));

        // Adform tracking.
        AdformTrackingSdk.onResume(this);

        registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Facebook event logging.
        AppEventsLogger.deactivateApp(this);

        // Adform tracking.
        AdformTrackingSdk.onPause();

        unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {

        boolean handled = false;

        // 1. check if the current fragment wants to go back:
        // Note to self, this mCurrentFragmentTagName scheme is flawed! Figure out why!
        if (mFragmentTagNameStack.size() != 0) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentByTag(mFragmentTagNameStack.peek());
            if (currentFragment != null) {
                handled = ((BaseFragment) currentFragment).onBackPress();
            }
        }

        if (!handled) {
            int count = getSupportFragmentManager().getBackStackEntryCount();

            // 2. Let the system handle it if we are the last fragment on the stack else pop
            // this fragment off the stack.
            if (count == 0) {
                super.onBackPressed();
            } else {
                mFragmentTagNameStack.pop();
                getSupportFragmentManager().popBackStack();
            }
        }

        setOrientationLock();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST: {
                Logger.d("Location: onRequestPermissionResult");
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    tryGetLocation();
                } else {
                    location = null;
                    proceedWithLocationUnAvailable();

                }
                return;
            }
        }
    }

    protected void tryGetLocation() {
        Log.d("MaxApp", "ExpressActivity tryGetLocation()");
        // Check if permission is granted.
        int permissionCheckOK = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheckOK == PackageManager.PERMISSION_GRANTED) {
            Logger.d("Location: permission granted.");
            GlobalState.appendLogText("Location: permission granted.");
            // Acquire a reference to the system Location Manager
            LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

            // Setup and listen for location updates.
            boolean gpsProviderEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkProviderEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (gpsProviderEnabled) {
                Logger.d("Location: gpsProvider enabled: Listening for gps location updates.");
                GlobalState.appendLogText("Location: gpsProvider enabled: Listening for gps location updates.");
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
                showUndismissibleDialog(R.string.location_fix_dialog_title, R.string.location_fix_dialog_message_gps);
            } else if (networkProviderEnabled) {
                Logger.d("Location: networkProvider enabled: Listening for network location updates.");
                GlobalState.appendLogText("Location: networkProvider enabled: Listening for network location updates.");
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                showUndismissibleDialog(R.string.location_fix_dialog_title, R.string.location_fix_dialog_message_network);

            } else {
                Logger.d("Location: all providers disabled: proceed with location unavailable.");
                GlobalState.appendLogText("Location: all providers disabled: proceed with location unavailable.");
                location = null;
                proceedWithLocationUnAvailable();
            }

        } else {
            Logger.d("Location: permission not granted. Request permissions.");
            GlobalState.appendLogText("Location: permission not granted. Request permissions.");
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    LOCATION_REQUEST);
        }
    }


    public void popBackStack() {
        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count > 0) {
            mFragmentTagNameStack.pop();
            getSupportFragmentManager().popBackStackImmediate();
        }

        setOrientationLock();
    }


     public void replaceCurrentFragmentWith(BaseFragment fragment, boolean backstack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack.
        mFragmentTagNameStack.push(fragment.getTagName());
        transaction.replace(R.id.fragment_container, fragment, mFragmentTagNameStack.peek());
        if (backstack) {
            transaction.addToBackStack(null);
        }

        // Commit the transaction.
        transaction.commit();

         setOrientationLock();
    }

    public void setOrientationLock() {
        Logger.d("BaseActivity setOrientationLock");
        if (isWebFragmentOnTop()) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    public boolean isWebFragmentOnTop() {
        if (mFragmentTagNameStack != null && !mFragmentTagNameStack.isEmpty()) {
            return mFragmentTagNameStack.peek().startsWith(WebFragment.class.getSimpleName());
        }

         return false;
    }


    public void handleError(se.max.android.locator.data.api.max.model.Error error, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative) {
        if (error == null) {

            showDialog(
                    R.string.io_error_title,
                    R.string.io_error_message,
                    R.string.button_retry,
                    R.string.button_cancel,
                    positive,
                    negative,
                    true);
        } else {
            showDialog(
                    error.getTitle(),
                    error.getMessage(),
                    R.string.button_retry,
                    R.string.button_cancel,
                    positive,
                    negative,
                    true);
        }
    }

    private Intent makeGotoApplicationSettingsIntent() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    private Intent makeGotoLocationSettingsIntent() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    private void setLocation(Location location) {
        Logger.d("Location: based on " + location.getProvider() + " " + location.toString());
        this.location = location;
        proceedWithLocationAvailable();
    }

    public Location getLocation() {
        return location;
    }


    public Stack<String> getFragmentTagNameStack() {
        return mFragmentTagNameStack;
    }

    public void showUndismissibleDialog(int title, int message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        dialog = builder.create();
        dialog.show();
    }

    public void showDialog(int title, int message, int positiveButton, int negativeButton, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative, boolean dismissable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveButton, positive);
        builder.setNegativeButton(negativeButton, negative);
        builder.setCancelable(dismissable);
        builder.create().show();
    }

    public void showDialog(String title, String message, int positiveButton, int negativeButton, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative, boolean dismissable) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton(positiveButton, positive);
        builder.setNegativeButton(negativeButton, negative);
        builder.setCancelable(dismissable);
        builder.create().show();
    }

    // ConnectivityChangeReceiver.ConnectivityInteractions implementation.
    @Override
    public void onNetworkAvailable() {
        Logger.d("Network available.");
        if (snackbar != null) {
            snackbar.dismiss();
        }

    }

    @Override
    public void onNetworkUnAvailable() {
        Logger.d("Network unavailable");
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), R.string.no_network_alert, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.button_ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                });
        snackbar.show();
    }

    public void proceedWithLocationAvailable() {
    }

    public void proceedWithLocationUnAvailable() {
    }

    // Base implementation of WebFragment.WebFragmentInteractions.
    @Override
    public void onError(se.max.android.locator.data.api.max.model.Error error, DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative) {
        handleError(error, positive, negative);
    }

    @Override
    public void onOpenUrl(String url) {

    }

    @Override
    public void onClosePage() {

    }

    @Override
    public void onCloseMaxPage() {

    }

    @Override
    public void onLogFacebookEvent(String event) {
        AppEventsLogger logger = AppEventsLogger.newLogger(this);
        logger.logEvent(event);
    }

    @Override
    public void onOpenNavigation() {

    }

    @Override
    public void onProgressChanged(int progress) {

    }

    @Override
    public void onPageLoaded(String url) {

    }

    @Override
    public void onOpenSettings() {
        // Check if permission is granted.
        int permissionCheckOK = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        if (permissionCheckOK == PackageManager.PERMISSION_GRANTED) {
            startActivity(makeGotoLocationSettingsIntent()); // Permission is granted. Open up location settings.
        } else {
            startActivity(makeGotoApplicationSettingsIntent()); // Permission is not granted. Open up permission settings.
        }
    }

    @Override
    public void onVibrate() {
        // Get instance of Vibrator from current Context
        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        // Start without a delay
        // Vibrate for 100 milliseconds
        // Sleep for 1000 milliseconds
        long[] pattern = {0, 500, 200, 500, 200, 500};

        // The '0' here means to repeat indefinitely
        // '0' is actually the index at which the pattern keeps repeating from (the start)
        // To repeat the pattern from any other point, you could increase the index, e.g. '1'
        v.vibrate(pattern, -1);
    }

    @Override
    public void onFindMax() {

    }

    @Override
    public void onPlaySound() {
        MediaPlayer mediaPlayer = MediaPlayer.create(this, R.raw.goodtogo);
        mediaPlayer.start();
    }

    @Override
    public String onAppendUrl(String url) {
        return url;
    }

    @Override
    public void onBankIdMissing() {
        snackbar = Snackbar
                .make(findViewById(android.R.id.content), R.string.no_bankid_alert, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

}
