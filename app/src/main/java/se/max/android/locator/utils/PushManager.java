package se.max.android.locator.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

import se.max.android.locator.data.api.max.model.push.Push;

/**
 * Created by pqv on 08/12/15.
 */
public class PushManager {

    private static final String PREFS_PUSH = "PrefsPush";
    private static final String SET_CONSUMED_KEY = "SetConsumedKey";

    private PushManager() {

    }

    public static boolean isConsumed(Context context, Push push) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_PUSH, Context.MODE_PRIVATE);

        Set<String> set = prefs.getStringSet(SET_CONSUMED_KEY, new HashSet<String>());

        return set.contains(push.getId());
    }

    public static void setConsumed(Context context, Push push) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_PUSH, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        Set<String> set = new HashSet<>(prefs.getStringSet(SET_CONSUMED_KEY, new HashSet<String>()));
        set.add(push.getId());
        editor.putStringSet(SET_CONSUMED_KEY, set);
        editor.commit();
    }
}
